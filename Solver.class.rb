# require "set"
require 'thread'

class Solver
  attr_accessor :open_lst
  # if closed_list[str] = {structure : [move for solution -1, cost]} (case where search for solution)
  # if closed_list[str] = {structure : [move for solution +1, cost]} (case where search for initial str)
  attr_accessor :closed_lst
  attr_accessor :is_solve
  attr_accessor :mode

  def initialize(str1, str2, last_move1, last_move2, mode)
    @open_lst = Array.new
    @closed_lst = Array.new
    @is_solve = false
    @mode = mode
    # if mode == 3
    #   @open_lst.insert(-1, get_state(str1, 1, last_move1))
    # else
      @open_lst.insert(-1, get_state(str1, 1, last_move1))
      @open_lst.insert(-1, get_state(str2, 1, last_move2))
    # end
  end

  #
  # solver 1
  #

  def get_state(str, cost, last_move)
    state = Struct.new(:str, :cost, :last_move) # status : 0, 1 (true, false)

    state.new(str, cost, last_move)
  end

  def check_open_lst(str, cost)
    return false if @open_lst.empty?

    i = 0
    j = @open_lst.count
    while i < j
      state = @open_lst[i]
      if (state.str <=> str) == 0 && state.cost > cost
        @open_lst.delete(@open_lst[i])
        j = @open_lst.count
      elsif (state.str <=> str) == 0 && state.cost <= cost
        return true
      else
        i += 1
      end
    end

    false
  end

  def check_closed_lst(str, cost)
    return false if @closed_lst.empty?

    i = 0
    j = @closed_lst.count
    while i < j
      state = @closed_lst[i]
      # if state.str <=> str && state.cost > cost
      #   @closed_lst.delete(@closed_lst[i])
      #   j = @closed_lst.count
      # elsif state.str <=> str && state.cost <= cost
      if (state.str <=> str) == 0
        return true
      end
      # else
        i += 1
    end

    false
  end

  def check_if_solve(str)
    if @mode == 0
      return true if (str <=> $g_good_cube_str) == 0
    elsif @mode == 3
      return true if (str <=> $g_good_cube_str) == 0
    else
      return true if (str <=> $g_initial_cube_str) == 0
    end

    false
  end

  def do_move(state, move, inv_move)
    return true if @is_solve == true
    return true if state.last_move == inv_move

    str = state.str.dup
    $g_move.apply_move(move, str)
    return true if (check_open_lst(str, state.cost + 1) == true) || (check_closed_lst(str, state.cost + 1) == true)

    if check_if_solve(str) == false
      @open_lst.insert(-1, get_state(str, state.cost + 1, move))
    else
      @closed_lst.insert(-1, get_state(str, state.cost + 1, move))
      @is_solve = true
      set_cost_solution(state.cost + 1)
      return false
    end

    true
  end

  def do_all_move(state)
    ret = true
    ret = do_move(state, Move_type::R, Move_type::Ri)
    return if ret == false
    ret = do_move(state, Move_type::Ri, Move_type::R)
    return if ret == false
    ret = do_move(state, Move_type::L, Move_type::Li)
    return if ret == false
    ret = do_move(state, Move_type::Li, Move_type::L)
    return if ret == false
    ret = do_move(state, Move_type::U, Move_type::Ui)
    return if ret == false
    ret = do_move(state, Move_type::Ui, Move_type::U)
    return if ret == false
    ret = do_move(state, Move_type::D, Move_type::Di)
    return if ret == false
    ret = do_move(state, Move_type::Di, Move_type::D)
    return if ret == false
    ret = do_move(state, Move_type::F, Move_type::Fi)
    return if ret == false
    ret = do_move(state, Move_type::Fi, Move_type::F)
    return if ret == false
    ret = do_move(state, Move_type::B, Move_type::Bi)
    return if ret == false
    ret = do_move(state, Move_type::Bi, Move_type::B)
    return if ret == false
  end

  def get_closed_with_str(str, cost)
    i = 0
    @closed_lst.each do |state|
      return i if (state.str <=> str) == 0 && state.cost == cost - 1
      i += 1
    end
  end

  def set_final_solution0()
    tmp_str = @closed_lst.last.str.dup
    tmp_cost = @closed_lst.last.cost
    $g_solution << @closed_lst.last.last_move
    $g_move.apply_move($g_move.invert_move(@closed_lst.last.last_move), tmp_str)
    while ($g_initial_cube_str <=> tmp_str) != 0
      i = get_closed_with_str(tmp_str, tmp_cost)
      $g_solution << @closed_lst[i].last_move
      $g_move.apply_move($g_move.invert_move(@closed_lst[i].last_move), tmp_str)
      tmp_cost = @closed_lst[i].cost
    end
    $g_solution.reverse!
    optimize_solution()
    optimize_solution_plus()
    $g_solution_move_nbr = $g_solution.count
  end

  def set_final_solution1()
    tmp_str = @closed_lst.last.str.dup
    tmp_cost = @closed_lst.last.cost
    $g_solution << $g_move.invert_move(@closed_lst.last.last_move)
    $g_move.apply_move($g_move.invert_move(@closed_lst.last.last_move), tmp_str)
    while ($g_good_cube_str <=> tmp_str) != 0
      i = get_closed_with_str(tmp_str, tmp_cost)
      $g_solution << $g_move.invert_move(@closed_lst[i].last_move)
      $g_move.apply_move($g_move.invert_move(@closed_lst[i].last_move), tmp_str)
      tmp_cost = @closed_lst[i].cost
    end
    optimize_solution()
    optimize_solution_plus()
    $g_solution_move_nbr = $g_solution.count
  end

  def set_cost_solution(cost)
    $g_semaphore.synchronize {
      if $g_solution_move_nbr == -1 || $g_solution_move_nbr > cost
        $g_solution_move_nbr = cost
        # puts "solution found, cost = " + cost.to_s + " mode = " + @mode.to_s
        set_final_solution0() if @mode == 0
        set_final_solution1() if @mode == 1
        # set_final_solution2() if @mode == 2 || @mode == 3
      end
    }
  end

  def check_continue(cost)
    state = true

    $g_semaphore.synchronize {
      # state = false if $g_solution_move_nbr != -1 && $g_solution_move_nbr <= cost
      state = false if $g_solution_move_nbr != -1
    }
    return false if state == false

    true
  end

  def add_elem_to_link_tmp(state, lst, sem)
    sem.synchronize {
      lst.insert(-1, state)
    }
  end

  def solve()
    return if @open_lst.empty?

    tmp_cost = 0
    while (@open_lst.count > 0) && (@is_solve == false)
      state = @open_lst.shift
      @closed_lst.insert(-1, state)
      if @mode == 0
        add_elem_to_link_tmp(state, $g_final_list_tmp0, $g_semaphore_link_tmp0)
      else
        add_elem_to_link_tmp(state, $g_final_list_tmp1, $g_semaphore_link_tmp1)
      end
      # puts state.cost
      tmp_cost = state.cost if state.cost > tmp_cost
      return if check_continue(tmp_cost) == false
      do_all_move(state)
      # return if check_continue(tmp_cost) == false
    end
  end

  def check_link_continue()
    state = true

    $g_semaphore.synchronize {
      state = false if $g_solution_move_nbr != -1
    }
    return false if state == false

    true
  end

  def get_link_state(state, lst, sem)
    i = 0

    sem.synchronize {
      while i < 50 && lst.count > 0
        state << lst.shift
        i += 1
      end
    }
  end

  def elem_link_compare(state, elem)
    i = 0

    while i < state.count
      if (state[i].str <=> elem.str) == 0
        # set_cost_solution(state[i].cost + elem.cost) # must uncomment
        # puts "solution found, cost = " + (state[i].cost + elem.cost).to_s + " mode = link"
        return true
      end
      i += 1
    end

    false
  end

  def check_link_list(state, lst, sem)
    i = 0

    sem.synchronize {
      while i < lst.count
        return true if elem_link_compare(state, lst[i]) == true
        i += 1
      end
    }

    false
  end

  def add_link_to_list(state, lst, sem)
    i = 0

    while i < state.count
      lst.insert(-1, state[i])
      i += 1
    end
  end

  def link0()
    state = Array.new

    while check_link_continue() == true
      get_link_state(state, $g_final_list_tmp0, $g_semaphore_link_tmp0)
      add_link_to_list(state, $g_final_list0, $g_semaphore_link0)
      return if state.count > 0 && check_link_list(state, $g_final_list1, $g_semaphore_link1) == true
      state.clear
    end
  end

  def link1()
    state = Array.new

    while check_link_continue() == true
      get_link_state(state, $g_final_list1, $g_semaphore_link_tmp1)
      add_link_to_list(state, $g_final_list1, $g_semaphore_link1)
      return if state.count > 0 && check_link_list(state, $g_final_list0, $g_semaphore_link0) == true
      state.clear
    end
  end

#
# solver 2
#

  def heuristic(str)
    val = 0
    for pos in 0...str.length
      val = val + 1 if $g_good_cube_str[pos]== str[pos]
    end

    str.length - val
  end

  def get_index_open_heuristic(cost)
    index = 0
    len = 0
    @open_lst.each do |elem|
      if cost < elem.cost
        return index
      else
        index = index + 1
      end
    end

    len - index
  end

  def insert_open_heuristic(state)
    # puts state.str, state.cost
    @open_lst.insert(get_index_open_heuristic(state.cost), state)
  end

  def do_move_heuristic(state, move, inv_move)
    return true if @is_solve == true
    return true if state.last_move == inv_move

    str = state.str.dup
    $g_move.apply_move(move, str)
    return true if (check_open_lst(str, heuristic(str)) == true) || (check_closed_lst(str, heuristic(str)) == true)

    if check_if_solve(str) == false
      insert_open_heuristic(get_state(str, heuristic(str), move))
    else
      @closed_lst.insert(-1, get_state(str, heuristic(str), move))
      @is_solve = true
      set_cost_solution(heuristic(str))
      puts "solution found"
      return false
    end

    true
  end

  def do_all_move_heuristic(state)
    ret = true
    ret = do_move_heuristic(state, Move_type::R, Move_type::Ri)
    return if ret == false
    ret = do_move_heuristic(state, Move_type::Ri, Move_type::R)
    return if ret == false
    ret = do_move_heuristic(state, Move_type::L, Move_type::Li)
    return if ret == false
    ret = do_move_heuristic(state, Move_type::Li, Move_type::L)
    return if ret == false
    ret = do_move_heuristic(state, Move_type::U, Move_type::Ui)
    return if ret == false
    ret = do_move_heuristic(state, Move_type::Ui, Move_type::U)
    return if ret == false
    ret = do_move_heuristic(state, Move_type::D, Move_type::Di)
    return if ret == false
    ret = do_move_heuristic(state, Move_type::Di, Move_type::D)
    return if ret == false
    ret = do_move_heuristic(state, Move_type::F, Move_type::Fi)
    return if ret == false
    ret = do_move_heuristic(state, Move_type::Fi, Move_type::F)
    return if ret == false
    ret = do_move_heuristic(state, Move_type::B, Move_type::Bi)
    return if ret == false
    ret = do_move_heuristic(state, Move_type::Bi, Move_type::B)
    return if ret == false
  end

  def solve_with_heuristic()
    return if @open_lst.empty?

    # tmp_cost = 0
    while (@open_lst.count > 0) && (@is_solve == false)
      state = @open_lst.shift
      @closed_lst.insert(-1, state)
      # puts state.cost
      # tmp_cost = state.cost if state.cost > tmp_cost
      # return if check_continue(tmp_cost) == false
      do_all_move_heuristic(state)
      # sleep(30)
      # return if check_continue(tmp_cost) == false
    end
  end

end
