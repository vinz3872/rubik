# 000000000111111111222222222333333333444444444555555555
module Pattern
	# ONE = ["_0_000_0_ _________ _2__2____ _3__3____ _4__4____ _5__5____", 1] # White cross
  ONE = ["_0_000_0___________2__2_____3__3_____4__4_____5__5____", 1] # White cross
	TWO = ["000000000_________222_2____333_3____444_4____555_5____", 2] # White face / first layer
  THREE = ["000000000_________222222___333333___444444___555555___", 3] # Middle layer
  FOUR = ["000000000_1_111_1_222222___333333___444444___555555___", 4] # Yellow cross
  FIVE = ["000000000111111111222222___333333___444444___555555___", 5] # Yellow face
  SIX = ["0000000001111111112222222_23333333_34444444_45555555_5", 6] # Good yellow corner
  # SEVEN = ["0000000001111111112222222_23333333_34444444_45555555_5", 7] # Good yellow edges
end

# W W W W W W W W W Y Y Y Y Y Y Y Y Y G G G G G G G G G B B B B B B B B B R R R R R R R R R O O O O O O O O O
#             default view
#                 back : R
#             ____ ____ ____
#           /_1_ /_2_ /_3_ /|
#         /___ /_W_ /___ /|3|
#       /    /    /    /|2|/|
#       ---- ---- ---- 1|/| |
#      | 1  | 2  | 3  |/|G|/|
#   B   ---- ---- ----  |/| |
#      |    | O  |    |/| |/
#       ---- ---- ----  |/
#      |    |    |    |/
#       ---- ---- ----
#            down : Y


module Basic_pattern
	# ONE = ["_________ _________ _________ _________ _________ _________", move_wrong_face, move_good_face, face]
	# ONE = ["WWWWWWWWW YYYYYYYYY GGGGGGGGG BBBBBBBBB RRRRRRRRR OOOOOOOOO", move_wrong_face, move_good_face, face
  # "OD" : default orientation, "OX" : orientation X, "DX" : down X time (edge)
	# White edge piece (Up)
	WE_U = [["____00_______1_____X__2________3________4________5____", ["OD", Move_type::R2, "DX", Move_type::R2, "OX", Move_type::R2], [], Face::G],
			  	["___00________1________2_____X__3________4________5____", ["OD", Move_type::L2, "DX", Move_type::L2, "OX", Move_type::R2], [], Face::B],
	        ["_0__0________1________2________3_____X__4________5____", ["OD", Move_type::B2, "DX", Move_type::B2, "OX", Move_type::R2], [], Face::R],
	        ["____0__0_____1________2________3________4_____X__5____", ["OD", Move_type::F2, "DX", Move_type::F2, "OX", Move_type::R2], [], Face::O],
	        ["____0X_______1_____0__2________3________4________5____", ["OD", Move_type::R2, "DX", Move_type::R2, "OX", Move_type::R, Move_type::U, Move_type::Fi, Move_type::Ui], [Move_type::Ri, Move_type::U, Move_type::Fi, Move_type::Ui], Face::G],
	        ["___X0________1________2_____0__3________4________5____", ["OD", Move_type::L2, "DX", Move_type::L2, "OX", Move_type::R, Move_type::U, Move_type::Fi, Move_type::Ui], [Move_type::Ri, Move_type::U, Move_type::Fi, Move_type::Ui], Face::B],
	        ["_X__0________1________2________3_____0__4________5____", ["OD", Move_type::B2, "DX", Move_type::B2, "OX", Move_type::R, Move_type::U, Move_type::Fi, Move_type::Ui], [Move_type::Ri, Move_type::U, Move_type::Fi, Move_type::Ui], Face::R],
	        ["____0__X_____1________2________3________4_____0__5____", ["OD", Move_type::F2, "DX", Move_type::F2, "OX", Move_type::R, Move_type::U, Move_type::Fi, Move_type::Ui], [Move_type::Ri, Move_type::U, Move_type::Fi, Move_type::Ui], Face::O]]
	# White edge piece (Middle)
	WE_M = [["____0________1_______X2________3________4________50___", ["OD", Move_type::Ri, "DX", Move_type::R, "OX", Move_type::R2], ["OD", Move_type::R], Face::G],
	        ["____0________1________20_______3_______X4________5____", ["OD", Move_type::Bi, "DX", Move_type::B, "OX", Move_type::R2], ["OD", Move_type::B], Face::R],
	        ["____0________1________2_______X3________40_______5____", ["OD", Move_type::Li, "DX", Move_type::L, "OX", Move_type::R2], ["OD", Move_type::L], Face::B],
	        ["____0________1________2________30_______4_______X5____", ["OD", Move_type::Fi, "DX", Move_type::F, "OX", Move_type::R2], ["OD", Move_type::F], Face::O],
          ["____0________1_______02________3________4________5X___", ["OD", Move_type::F, "DX", Move_type::Fi, "OX", Move_type::R2], ["OD", Move_type::Fi], Face::O],
        	["____0________1________2X_______3_______04________5____", ["OD", Move_type::R, "DX", Move_type::Ri, "OX", Move_type::R2], ["OD", Move_type::Ri], Face::G],
        	["____0________1________2_______03________4X_______5____", ["OD", Move_type::B, "DX", Move_type::Bi, "OX", Move_type::R2], ["OD", Move_type::Bi], Face::R],
        	["____0________1________2________3X_______4_______05____", ["OD", Move_type::L, "DX", Move_type::Li, "OX", Move_type::R2], ["OD", Move_type::Li], Face::B]]
	# White edge piece (Down)
	WE_D = [["____0_____0__1________2________3________4________5__X_", ["OD", "DX", "OX", Move_type::R2], ["OD", Move_type::F2], Face::O],
  	      ["____0________10_______2__X_____3________4________5____", ["OD", "DX", "OX", Move_type::R2], ["OD", Move_type::R2], Face::G],
  	      ["____0________1__0_____2________3________4__X_____5____", ["OD", "DX", "OX", Move_type::R2], ["OD", Move_type::B2], Face::R],
  	      ["____0_______01________2________3__X_____4________5____", ["OD", "DX", "OX", Move_type::R2], ["OD", Move_type::L2], Face::B],
  	      ["____0_____X__1________2________3________4________5__0_", ["OD", "DX", "OX", Move_type::R, Move_type::U, Move_type::Fi, Move_type::Ui], [Move_type::R, Move_type::U, Move_type::Fi, Move_type::Ui], Face::O],
  	      ["____0________1X_______2__0_____3________4________5____", ["OD", "DX", "OX", Move_type::R, Move_type::U, Move_type::Fi, Move_type::Ui], [Move_type::R, Move_type::U, Move_type::Fi, Move_type::Ui], Face::G],
  	      ["____0________1__X_____2________3________4__0_____5____", ["OD", "DX", "OX", Move_type::R, Move_type::U, Move_type::Fi, Move_type::Ui], [Move_type::R, Move_type::U, Move_type::Fi, Move_type::Ui], Face::R],
  	      ["____0_______X1________2________3__0_____4________5____", ["OD", "DX", "OX", Move_type::R, Move_type::U, Move_type::Fi, Move_type::Ui], [Move_type::R, Move_type::U, Move_type::Fi, Move_type::Ui], Face::B]]
  # white corner (Up) (X: face, Y: right, Z: up/down)
    # white corner (white up)
  WC_U = [["_0_000_0Z_________Y2__2_____3__3_____4__4_____5X_5____", ["OD", "OF", Move_type::Ri, Move_type::Di, Move_type::R, Move_type::D, "OX", "OD", "DX", "OX"], [], [Face::O, Face::G, Face::W], Face::O],
          ["_0Z000_0___________2X_2_____3__3____Y4__4_____5__5____", ["OD", "OF", Move_type::Ri, Move_type::Di, Move_type::R, Move_type::D, "OX", "OD", "DX", "OX"], [], [Face::G, Face::R, Face::W], Face::G],
          ["Z0_000_0___________2__2____Y3__3_____4X_4_____5__5____", ["OD", "OF", Move_type::Ri, Move_type::Di, Move_type::R, Move_type::D, "OX", "OD", "DX", "OX"], [], [Face::R, Face::B, Face::W], Face::R],
          ["_0_000Z0___________2__2_____3X_3_____4__4____Y5__5____", ["OD", "OF", Move_type::Ri, Move_type::Di, Move_type::R, Move_type::D, "OX", "OD", "DX", "OX"], [], [Face::B, Face::O, Face::W], Face::B]]
  # white corner (Down)
    # white corner (white down)
    # white corner (white right)
    # white corner (white face)
  WC_D = [["_0_000_0___0_______2__2_Y___3__3_____4__4_____5__5___X", ["OD", "DX", "OX", Move_type::Ri, Move_type::D2, Move_type::R, Move_type::D, Move_type::Ri, Move_type::Di, Move_type::R], [Move_type::Ri, Move_type::D2, Move_type::R, Move_type::D, Move_type::Ri, Move_type::Di, Move_type::R], [Face::G, Face::O, Face::W], Face::O],
          ["_0_000_0_________0_2__2___X_3__3_____4__4_Y___5__5____", ["OD", "DX", "OX", Move_type::Ri, Move_type::D2, Move_type::R, Move_type::D, Move_type::Ri, Move_type::Di, Move_type::R], [Move_type::Ri, Move_type::D2, Move_type::R, Move_type::D, Move_type::Ri, Move_type::Di, Move_type::R], [Face::R, Face::G, Face::W], Face::G],
          ["_0_000_0_______0___2__2_____3__3_Y___4__4___X_5__5____", ["OD", "DX", "OX", Move_type::Ri, Move_type::D2, Move_type::R, Move_type::D, Move_type::Ri, Move_type::Di, Move_type::R], [Move_type::Ri, Move_type::D2, Move_type::R, Move_type::D, Move_type::Ri, Move_type::Di, Move_type::R], [Face::B, Face::R, Face::W], Face::R],
          ["_0_000_0_0_________2__2_____3__3___X_4__4_____5__5_Y__", ["OD", "DX", "OX", Move_type::Ri, Move_type::D2, Move_type::R, Move_type::D, Move_type::Ri, Move_type::Di, Move_type::R], [Move_type::Ri, Move_type::D2, Move_type::R, Move_type::D, Move_type::Ri, Move_type::Di, Move_type::R], [Face::O, Face::B, Face::W], Face::B],
          ["_0_000_0___Z_______2__2_0___3__3_____4__4_____5__5___X", ["OD", "DX", "OX", Move_type::Ri, Move_type::Di, Move_type::R], [Move_type::Ri, Move_type::Di, Move_type::R], [Face::O, Face::W, Face::G], Face::O],
          ["_0_000_0_________Z_2__2___X_3__3_____4__4_0___5__5____", ["OD", "DX", "OX", Move_type::Ri, Move_type::Di, Move_type::R], [Move_type::Ri, Move_type::Di, Move_type::R], [Face::G, Face::W, Face::R], Face::G],
          ["_0_000_0_______Z___2__2_____3__3_0___4__4___X_5__5____", ["OD", "DX", "OX", Move_type::Ri, Move_type::Di, Move_type::R], [Move_type::Ri, Move_type::Di, Move_type::R], [Face::R, Face::W, Face::B], Face::R],
          ["_0_000_0_Z_________2__2_____3__3___X_4__4_____5__5_0__", ["OD", "DX", "OX", Move_type::Ri, Move_type::Di, Move_type::R], [Move_type::Ri, Move_type::Di, Move_type::R], [Face::B, Face::W, Face::O], Face::B],
          ["_0_000_0___Z_______2__2_Y___3__3_____4__4_____5__5___0", ["OD", "DX", "OX", Move_type::F, Move_type::D, Move_type::Fi], [Move_type::F, Move_type::D, Move_type::Fi], [Face::W, Face::G, Face::O], Face::O],
          ["_0_000_0_________Z_2__2___0_3__3_____4__4_Y___5__5____", ["OD", "DX", "OX", Move_type::F, Move_type::D, Move_type::Fi], [Move_type::F, Move_type::D, Move_type::Fi], [Face::W, Face::R, Face::G], Face::G],
          ["_0_000_0_______Z___2__2_____3__3_Y___4__4___0_5__5____", ["OD", "DX", "OX", Move_type::F, Move_type::D, Move_type::Fi], [Move_type::F, Move_type::D, Move_type::Fi], [Face::W, Face::B, Face::R], Face::R],
          ["_0_000_0_Z_________2__2_____3__3___0_4__4_____5__5_Y__", ["OD", "DX", "OX", Move_type::F, Move_type::D, Move_type::Fi], [Move_type::F, Move_type::D, Move_type::Fi], [Face::W, Face::O, Face::B], Face::B]]
  # Middle layer   x: face, y: up
    # edge Up
    ML_U = [["000000000_Y_______222_2____333_3____444_4____555_5__X_", ["UX", Move_type::U, Move_type::R, Move_type::Ui, Move_type::Ri, Move_type::Ui, Move_type::Fi, Move_type::U, Move_type::F], ["UX", Move_type::Ui, Move_type::Li, Move_type::U, Move_type::L, Move_type::U, Move_type::F, Move_type::Ui, Move_type::Fi], Face::O],
            ["000000000_____Y___222_2__X_333_3____444_4____555_5____", ["UX", Move_type::U, Move_type::R, Move_type::Ui, Move_type::Ri, Move_type::Ui, Move_type::Fi, Move_type::U, Move_type::F], ["UX", Move_type::Ui, Move_type::Li, Move_type::U, Move_type::L, Move_type::U, Move_type::F, Move_type::Ui, Move_type::Fi], Face::G],
            ["000000000_______Y_222_2____333_3____444_4__X_555_5____", ["UX", Move_type::U, Move_type::R, Move_type::Ui, Move_type::Ri, Move_type::Ui, Move_type::Fi, Move_type::U, Move_type::F], ["UX", Move_type::Ui, Move_type::Li, Move_type::U, Move_type::L, Move_type::U, Move_type::F, Move_type::Ui, Move_type::Fi], Face::R],
            ["000000000___Y_____222_2____333_3__X_444_4____555_5____", ["UX", Move_type::U, Move_type::R, Move_type::Ui, Move_type::Ri, Move_type::Ui, Move_type::Fi, Move_type::U, Move_type::F], ["UX", Move_type::Ui, Move_type::Li, Move_type::U, Move_type::L, Move_type::U, Move_type::F, Move_type::Ui, Move_type::Fi], Face::B]]
  # edge middle    x: face, y: right
  # ML_U = ["000000000_________222_2____333_3____444_4____555_5____", ]
    ML_M = [["000000000_________222Y2____333_3____444_4____555_5X___", [Move_type::U, Move_type::R, Move_type::Ui, Move_type::Ri, Move_type::Ui, Move_type::Fi, Move_type::U, Move_type::F], [], Face::O, Face::G],
            ["000000000_________222_2X___333_3____444Y4____555_5____", [Move_type::U, Move_type::R, Move_type::Ui, Move_type::Ri, Move_type::Ui, Move_type::Fi, Move_type::U, Move_type::F], [], Face::G, Face::R],
            ["000000000_________222_2____333Y3____444_4X___555_5____", [Move_type::U, Move_type::R, Move_type::Ui, Move_type::Ri, Move_type::Ui, Move_type::Fi, Move_type::U, Move_type::F], [], Face::R, Face::B],
            ["000000000_________222_2____333_3X___444_4____555Y5____", [Move_type::U, Move_type::R, Move_type::Ui, Move_type::Ri, Move_type::Ui, Move_type::Fi, Move_type::U, Move_type::F], [], Face::B, Face::O]]
    # ML_M = ["000000000 _________ 222_2____ 333_3____ 444_4____ 555_5____", ]
  # Yellow cross
  YCR = [["000000000___111___222222___333333___444444___555555___", [Move_type::F, Move_type::R, Move_type::U, Move_type::Ri, Move_type::Ui, Move_type::Fi], Face::O],
         ["000000000_1__1__1_222222___333333___444444___555555___", [Move_type::F, Move_type::R, Move_type::U, Move_type::Ri, Move_type::Ui, Move_type::Fi], Face::B],
         ["000000000_1_11____222222___333333___444444___555555___", [Move_type::F, Move_type::U, Move_type::R, Move_type::Ui, Move_type::Ri, Move_type::Fi], Face::R],
         ["000000000_1__11___222222___333333___444444___555555___", [Move_type::F, Move_type::U, Move_type::R, Move_type::Ui, Move_type::Ri, Move_type::Fi], Face::B],
         ["000000000____11_1_222222___333333___444444___555555___", [Move_type::F, Move_type::U, Move_type::R, Move_type::Ui, Move_type::Ri, Move_type::Fi], Face::O],
         ["000000000___11__1_222222___333333___444444___555555___", [Move_type::F, Move_type::U, Move_type::R, Move_type::Ui, Move_type::Ri, Move_type::Fi], Face::G],
         ["000000000____1____222222___333333___444444___555555___", [Move_type::F, Move_type::U, Move_type::R, Move_type::Ui, Move_type::Ri, Move_type::Fi], Face::O]]
  # Yellow face
    # 2 wrong corner
  YF2 = [["000000000X1X111X1X222222___333333___444444___555555__1", [Move_type::R, Move_type::U, Move_type::Ri, Move_type::U, Move_type::R, Move_type::U2, Move_type::Ri], Face::O],
         ["000000000X1X111X1X222222__1333333___444444___555555___", [Move_type::R, Move_type::U, Move_type::Ri, Move_type::U, Move_type::R, Move_type::U2, Move_type::Ri], Face::G],
         ["000000000X1X111X1X222222___333333___444444__1555555___", [Move_type::R, Move_type::U, Move_type::Ri, Move_type::U, Move_type::R, Move_type::U2, Move_type::Ri], Face::R],
         ["000000000X1X111X1X222222___333333__1444444___555555___", [Move_type::R, Move_type::U, Move_type::Ri, Move_type::U, Move_type::R, Move_type::U2, Move_type::Ri], Face::B]]
    # 1 good corner and 0 corner
  YF = [["00000000011_111_1_222222___333333___444444___555555___", [Move_type::R, Move_type::U, Move_type::Ri, Move_type::U, Move_type::R, Move_type::U2, Move_type::Ri], Face::B],
        ["000000000_11111_1_222222___333333___444444___555555___", [Move_type::R, Move_type::U, Move_type::Ri, Move_type::U, Move_type::R, Move_type::U2, Move_type::Ri], Face::O],
        ["000000000_1_11111_222222___333333___444444___555555___", [Move_type::R, Move_type::U, Move_type::Ri, Move_type::U, Move_type::R, Move_type::U2, Move_type::Ri], Face::R],
        ["000000000_1_111_11222222___333333___444444___555555___", [Move_type::R, Move_type::U, Move_type::Ri, Move_type::U, Move_type::R, Move_type::U2, Move_type::Ri], Face::G],
        ["000000000_1_111_1_2222221__333333___444444___555555___", [Move_type::R, Move_type::U, Move_type::Ri, Move_type::U, Move_type::R, Move_type::U2, Move_type::Ri], Face::O],
        ["000000000_1_111_1_222222___333333___4444441__555555___", [Move_type::R, Move_type::U, Move_type::Ri, Move_type::U, Move_type::R, Move_type::U2, Move_type::Ri], Face::G],
        ["000000000_1_111_1_222222___3333331__444444___555555___", [Move_type::R, Move_type::U, Move_type::Ri, Move_type::U, Move_type::R, Move_type::U2, Move_type::Ri], Face::R],
        ["000000000_1_111_1_222222___333333___444444___5555551__", [Move_type::R, Move_type::U, Move_type::Ri, Move_type::U, Move_type::R, Move_type::U2, Move_type::Ri], Face::B]]
  # Good yellow corner
  GYC = [Move_type::Ri, Move_type::F, Move_type::Ri, Move_type::B2, Move_type::R, Move_type::Fi, Move_type::Ri, Move_type::B2, Move_type::R2, Move_type::Ui]
  # Final layer
  FL_R = [Move_type::F2, Move_type::U, Move_type::L, Move_type::Ri, Move_type::F2, Move_type::Li, Move_type::R, Move_type::U, Move_type::F2]
  FL_L = [Move_type::F2, Move_type::Ui, Move_type::L, Move_type::Ri, Move_type::F2, Move_type::Li, Move_type::R, Move_type::Ui, Move_type::F2]

end
