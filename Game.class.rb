#!/usr/bin/env ruby

load 'Render.class.rb'
load 'Move.class.rb'
load 'Pattern.rb'
load 'Solver.class.rb'
load 'Parser.class.rb'
load 'Cube.class.rb'
load 'Pattern_search.class.rb'

def cube_str_compare(str, find)
  for pos in 0...str.length
    return false if find[pos] != '_' && find[pos] != str[pos]
  end

  true
end

def serialize(cube, size)
  str = ""
  i = 0

  while i < 6
    j = 0
    while j < size
      k = 0
      while k < size
        str << cube[i][j][k].to_s
        k += 1
      end
      j += 1
    end
    i += 1
  end

  str
end

def deserialize(str, size)
  cube = Cube.new(size)
  tmp = 0
  i = 0
  while i < 6
    j = 0
    while j < size
      k = 0
      while k < size
        cube.cube[i][j][k] = str[tmp].to_i
        tmp += 1
        k += 1
      end
      j += 1
    end
    i += 1
  end

  cube
end

def get_new_solver_with_move(str, move1, move2, mode) # mode == 1 : inital -> solution, mode == 2 solution -> initial
  str1 = str.dup
  str2 = str.dup
  $g_move.apply_move(move1, str1)
  $g_move.apply_move(move2, str2)

  Solver.new(str1, str2, move1, move2, mode)
end

def optimize_solution_plus()
  return if $g_solution.count <= 1
  tmp_move = $g_solution[0]
  tmp_move_index = 0
  max = $g_solution.count
  i = 1
  while i <= max
    tmp = i
    while tmp <= max
      case tmp_move
      when Move_type::R
        if $g_solution[tmp] == Move_type::R
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::R2
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::Ri
          $g_solution.delete_at(tmp)
          $g_solution.delete_at(tmp_move_index)
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::R2
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::Ri
          return optimize_solution_plus()
        elsif $g_solution[tmp] != Move_type::L && $g_solution[tmp] != Move_type::Li && $g_solution[tmp] != Move_type::L2
          tmp = max + 1
        end
      when Move_type::Ri
        if $g_solution[tmp] == Move_type::Ri
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::R2
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::R
          $g_solution.delete_at(tmp)
          $g_solution.delete_at(tmp_move_index)
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::R2
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::R
          return optimize_solution_plus()
        elsif $g_solution[tmp] != Move_type::L && $g_solution[tmp] != Move_type::Li && $g_solution[tmp] != Move_type::L2
          tmp = max + 1
        end

      when Move_type::L
        if $g_solution[tmp] == Move_type::L
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::L2
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::Li
          $g_solution.delete_at(tmp)
          $g_solution.delete_at(tmp_move_index)
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::L2
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::Li
          return optimize_solution_plus()
        elsif $g_solution[tmp] != Move_type::R && $g_solution[tmp] != Move_type::Ri && $g_solution[tmp] != Move_type::R2
          tmp = max + 1
        end
      when Move_type::Li
        if $g_solution[tmp] == Move_type::Li
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::L2
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::L
          $g_solution.delete_at(tmp)
          $g_solution.delete_at(tmp_move_index)
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::L2
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::L
          return optimize_solution_plus()
        elsif $g_solution[tmp] != Move_type::R && $g_solution[tmp] != Move_type::Ri && $g_solution[tmp] != Move_type::R2
          tmp = max + 1
        end

      when Move_type::F
        if $g_solution[tmp] == Move_type::F
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::F2
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::Fi
          $g_solution.delete_at(tmp)
          $g_solution.delete_at(tmp_move_index)
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::F2
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::Fi
          return optimize_solution_plus()
        elsif $g_solution[tmp] != Move_type::B && $g_solution[tmp] != Move_type::Bi && $g_solution[tmp] != Move_type::B2
          tmp = max + 1
        end
      when Move_type::Fi
        if $g_solution[tmp] == Move_type::Fi
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::F2
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::F
          $g_solution.delete_at(tmp)
          $g_solution.delete_at(tmp_move_index)
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::F2
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::F
          return optimize_solution_plus()
        elsif $g_solution[tmp] != Move_type::B && $g_solution[tmp] != Move_type::Bi && $g_solution[tmp] != Move_type::B2
          tmp = max + 1
        end

      when Move_type::B
        if $g_solution[tmp] == Move_type::B
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::B2
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::Bi
          $g_solution.delete_at(tmp)
          $g_solution.delete_at(tmp_move_index)
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::B2
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::Bi
          return optimize_solution_plus()
        elsif $g_solution[tmp] != Move_type::F && $g_solution[tmp] != Move_type::Fi && $g_solution[tmp] != Move_type::F2
          tmp = max + 1
        end
      when Move_type::Bi
        if $g_solution[tmp] == Move_type::Bi
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::B2
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::B
          $g_solution.delete_at(tmp)
          $g_solution.delete_at(tmp_move_index)
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::B2
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::B
          return optimize_solution_plus()
        elsif $g_solution[tmp] != Move_type::F && $g_solution[tmp] != Move_type::Fi && $g_solution[tmp] != Move_type::F2
          tmp = max + 1
        end

      when Move_type::U
        if $g_solution[tmp] == Move_type::U
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::U2
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::Ui
          $g_solution.delete_at(tmp)
          $g_solution.delete_at(tmp_move_index)
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::U2
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::Ui
          return optimize_solution_plus()
        elsif $g_solution[tmp] != Move_type::D && $g_solution[tmp] != Move_type::Di && $g_solution[tmp] != Move_type::D2
          tmp = max + 1
        end
      when Move_type::Ui
        if $g_solution[tmp] == Move_type::Ui
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::U2
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::U
          $g_solution.delete_at(tmp)
          $g_solution.delete_at(tmp_move_index)
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::U2
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::U
          return optimize_solution_plus()
        elsif $g_solution[tmp] != Move_type::D && $g_solution[tmp] != Move_type::Di && $g_solution[tmp] != Move_type::D2
          tmp = max + 1
        end

      when Move_type::D
        if $g_solution[tmp] == Move_type::D
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::D2
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::Di
          $g_solution.delete_at(tmp)
          $g_solution.delete_at(tmp_move_index)
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::D2
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::Di
          return optimize_solution_plus()
        elsif $g_solution[tmp] != Move_type::U && $g_solution[tmp] != Move_type::Ui && $g_solution[tmp] != Move_type::U2
          tmp = max + 1
        end
      when Move_type::Di
        if $g_solution[tmp] == Move_type::Di
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::D2
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::D
          $g_solution.delete_at(tmp)
          $g_solution.delete_at(tmp_move_index)
          return optimize_solution_plus()
        elsif $g_solution[tmp] == Move_type::D2
          $g_solution.delete_at(tmp)
          $g_solution[tmp_move_index] = Move_type::D
          return optimize_solution_plus()
        elsif $g_solution[tmp] != Move_type::U && $g_solution[tmp] != Move_type::Ui && $g_solution[tmp] != Move_type::U2
          tmp = max + 1
        end

      end
      tmp += 1
    end
    tmp_move = $g_solution[i]
    tmp_move_index = i
    i += 1
  end

  true
end

def optimize_solution()
  return if $g_solution.count <= 1

  i = $g_solution.count - 1
  move_tmp = $g_solution[$g_solution.count]
  while i >= 0
    if (move_tmp == Move_type::R && $g_solution[i] == Move_type::R) || (move_tmp == Move_type::Ri && $g_solution[i] == Move_type::Ri)
      $g_solution.delete_at(i + 1)
      $g_solution[i] = Move_type::R2
      return optimize_solution()
    elsif (move_tmp == Move_type::Ri && $g_solution[i] == Move_type::R) || (move_tmp == Move_type::R && $g_solution[i] == Move_type::Ri)
      $g_solution.delete_at(i)
      $g_solution.delete_at(i)
      return optimize_solution()
    elsif (move_tmp == Move_type::R2 && $g_solution[i] == Move_type::R) || (move_tmp == Move_type::R && $g_solution[i] == Move_type::R2)
      $g_solution.delete_at(i + 1)
      $g_solution[i] = Move_type::Ri
      return optimize_solution()
    elsif (move_tmp == Move_type::R2 && $g_solution[i] == Move_type::Ri) || (move_tmp == Move_type::Ri && $g_solution[i] == Move_type::R2)
      $g_solution.delete_at(i + 1)
      $g_solution[i] = Move_type::R
      return optimize_solution()

    elsif (move_tmp == Move_type::L && $g_solution[i] == Move_type::L) || (move_tmp == Move_type::Li && $g_solution[i] == Move_type::Li)
      $g_solution.delete_at(i + 1)
      $g_solution[i] = Move_type::L2
      return optimize_solution()
    elsif (move_tmp == Move_type::Li && $g_solution[i] == Move_type::L) || (move_tmp == Move_type::L && $g_solution[i] == Move_type::Li)
      $g_solution.delete_at(i)
      $g_solution.delete_at(i)
      return optimize_solution()
    elsif (move_tmp == Move_type::L2 && $g_solution[i] == Move_type::L) || (move_tmp == Move_type::L && $g_solution[i] == Move_type::L2)
      $g_solution.delete_at(i + 1)
      $g_solution[i] = Move_type::Li
      return optimize_solution()
    elsif (move_tmp == Move_type::L2 && $g_solution[i] == Move_type::Li) || (move_tmp == Move_type::Li && $g_solution[i] == Move_type::L2)
      $g_solution.delete_at(i + 1)
      $g_solution[i] = Move_type::L
      return optimize_solution()

    elsif (move_tmp == Move_type::U && $g_solution[i] == Move_type::U) || (move_tmp == Move_type::Ui && $g_solution[i] == Move_type::Ui)
      $g_solution.delete_at(i + 1)
      $g_solution[i] = Move_type::U2
      return optimize_solution()
    elsif (move_tmp == Move_type::Ui && $g_solution[i] == Move_type::U) || (move_tmp == Move_type::U && $g_solution[i] == Move_type::Ui)
      $g_solution.delete_at(i)
      $g_solution.delete_at(i)
      return optimize_solution()
    elsif (move_tmp == Move_type::U2 && $g_solution[i] == Move_type::U) || (move_tmp == Move_type::U && $g_solution[i] == Move_type::U2)
      $g_solution.delete_at(i + 1)
      $g_solution[i] = Move_type::Ui
      return optimize_solution()
    elsif (move_tmp == Move_type::U2 && $g_solution[i] == Move_type::Ui) || (move_tmp == Move_type::Ui && $g_solution[i] == Move_type::U2)
      $g_solution.delete_at(i + 1)
      $g_solution[i] = Move_type::U
      return optimize_solution()

    elsif (move_tmp == Move_type::D && $g_solution[i] == Move_type::D) || (move_tmp == Move_type::Di && $g_solution[i] == Move_type::Di)
      $g_solution.delete_at(i + 1)
      $g_solution[i] = Move_type::D2
      return optimize_solution()
    elsif (move_tmp == Move_type::Di && $g_solution[i] == Move_type::D) || (move_tmp == Move_type::D && $g_solution[i] == Move_type::Di)
      $g_solution.delete_at(i)
      $g_solution.delete_at(i)
      return optimize_solution()
    elsif (move_tmp == Move_type::D2 && $g_solution[i] == Move_type::D) || (move_tmp == Move_type::D && $g_solution[i] == Move_type::D2)
      $g_solution.delete_at(i + 1)
      $g_solution[i] = Move_type::Di
      return optimize_solution()
    elsif (move_tmp == Move_type::D2 && $g_solution[i] == Move_type::Di) || (move_tmp == Move_type::Di && $g_solution[i] == Move_type::D2)
      $g_solution.delete_at(i + 1)
      $g_solution[i] = Move_type::D
      return optimize_solution()

    elsif (move_tmp == Move_type::F && $g_solution[i] == Move_type::F) || (move_tmp == Move_type::Fi && $g_solution[i] == Move_type::Fi)
      $g_solution.delete_at(i + 1)
      $g_solution[i] = Move_type::F2
      return optimize_solution()
    elsif (move_tmp == Move_type::Fi && $g_solution[i] == Move_type::F) || (move_tmp == Move_type::F && $g_solution[i] == Move_type::Fi)
      $g_solution.delete_at(i)
      $g_solution.delete_at(i)
      return optimize_solution()
    elsif (move_tmp == Move_type::F2 && $g_solution[i] == Move_type::F) || (move_tmp == Move_type::F && $g_solution[i] == Move_type::F2)
      $g_solution.delete_at(i + 1)
      $g_solution[i] = Move_type::Fi
      return optimize_solution()
    elsif (move_tmp == Move_type::F2 && $g_solution[i] == Move_type::Fi) || (move_tmp == Move_type::Fi && $g_solution[i] == Move_type::F2)
      $g_solution.delete_at(i + 1)
      $g_solution[i] = Move_type::F
      return optimize_solution()

    elsif (move_tmp == Move_type::B && $g_solution[i] == Move_type::B) || (move_tmp == Move_type::Bi && $g_solution[i] == Move_type::Bi)
      $g_solution.delete_at(i + 1)
      $g_solution[i] = Move_type::B2
      return optimize_solution()
    elsif (move_tmp == Move_type::Bi && $g_solution[i] == Move_type::B) || (move_tmp == Move_type::B && $g_solution[i] == Move_type::Bi)
      $g_solution.delete_at(i)
      $g_solution.delete_at(i)
      return optimize_solution()
    elsif (move_tmp == Move_type::B2 && $g_solution[i] == Move_type::B) || (move_tmp == Move_type::B && $g_solution[i] == Move_type::B2)
      $g_solution.delete_at(i + 1)
      $g_solution[i] = Move_type::Bi
      return optimize_solution()
    elsif (move_tmp == Move_type::B2 && $g_solution[i] == Move_type::Bi) || (move_tmp == Move_type::Bi && $g_solution[i] == Move_type::B2)
      $g_solution.delete_at(i + 1)
      $g_solution[i] = Move_type::B
      return optimize_solution()

    else
      move_tmp = $g_solution[i]
      i -= 1
    end

    true
  end
end

def move_to_str(move)
  case move
  when Move_type::R
    return "R"
  when Move_type::L
    return "L"
  when Move_type::U
    return "U"
  when Move_type::D
    return "D"
  when Move_type::F
    return "F"
  when Move_type::B
    return "B"
  when Move_type::Ri
    return "Ri"
  when Move_type::Li
    return "Li"
  when Move_type::Ui
    return "Ui"
  when Move_type::Di
    return "Di"
  when Move_type::Fi
    return "Fi"
  when Move_type::Bi
    return "Bi"
  when Move_type::R2
    return "R2"
  when Move_type::L2
    return "L2"
  when Move_type::U2
    return "U2"
  when Move_type::D2
    return "D2"
  when Move_type::F2
    return "F2"
  when Move_type::B2
    return "B2"
  end
end

def print_move_list()
  $g_solution.each do |move|
    print move_to_str(move)
    print " "
  end
  puts "", "Done in " + $g_solution_move_nbr.to_s + " moves"
end

def generate_mix_str(size)
  gen = Random.new()
  str = ""
  i = 0
  while i < size
    nbr = gen.rand(18)
    case nbr
    when 0
      str << " R"
    when 1
      str << " L"
    when 2
      str << " U"
    when 3
      str << " D"
    when 4
      str << " F"
    when 5
      str << " B"
    when 6
      str << " R'"
    when 7
      str << " L'"
    when 8
      str << " U'"
    when 9
      str << " D'"
    when 10
      str << " F'"
    when 11
      str << " B'"
    when 12
      str << " R2"
    when 13
      str << " L2"
    when 14
      str << " U2"
    when 15
      str << " D2"
    when 16
      str << " F2"
    when 17
      str << " B2"
    when 18
      puts "NO"
    end
    i += 1
  end

  str
end

def print_subdivision()
  state = 0
  str = $g_initial_cube_str.dup
  sol = ""
  i = 0
  tmp = [0, 0, 0, 0, 0, 0, 0]
  puts "\nMove to solve the White cross:"
  while state != 7
    $g_move.apply_move($g_solution[i], str)
    sol << move_to_str($g_solution[i])
    sol << " "
    i += 1
    state = 1 if cube_str_compare(str, Pattern::ONE[0]) == true
    if state == 1 && tmp[state - 1] == 0
      tmp[state - 1] = 1
      puts sol if sol.length > 0
      sol = ""
      puts "\nMove to solve the first layer (White face):"
    end
    state = 2 if cube_str_compare(str, Pattern::TWO[0]) == true
    if state == 2 && tmp[state - 1] == 0
      tmp[state - 1] = 1
      puts sol if sol.length > 0
      sol = ""
      puts "\nMove to solve the Middle layer:"
    end
    state = 3 if cube_str_compare(str, Pattern::THREE[0]) == true
    if state == 3 && tmp[state - 1] == 0
      tmp[state - 1] = 1
      puts sol if sol.length > 0
      sol = ""
      puts "\nMove to solve the Yellow cross:"
    end
    state = 4 if cube_str_compare(str, Pattern::FOUR[0]) == true
    if state == 4 && tmp[state - 1] == 0
      tmp[state - 1] = 1
      puts sol if sol.length > 0
      sol = ""
      puts "\nMove to solve the Yellow face:"
    end
    state = 5 if cube_str_compare(str, Pattern::FIVE[0]) == true
    if state == 5 && tmp[state - 1] == 0
      tmp[state - 1] = 1
      puts sol if sol.length > 0
      sol = ""
      puts "\nMove to solve the Yellow corner:"
    end
    state = 6 if cube_str_compare(str, Pattern::SIX[0]) == true
    if state == 6 && tmp[state - 1] == 0
      tmp[state - 1] = 1
      puts sol if sol.length > 0
      sol = ""
      puts "\nMove to finish the cube:"
    end
    state = 7 if cube_str_compare(str, $g_good_cube_str) == true
    if state == 7 && tmp[state - 1] == 0
      tmp[state - 1] = 1
      puts sol if sol.length > 0
      sol = ""
    end
  end
end

def solve_pattern(str)
  pattern_solver = Pattern_search.new(str)
  pattern_solver.solve()
end

def solve_force()
  threads = []

  threads << Thread.new { solve = get_new_solver_with_move($g_initial_cube_str, Move_type::R, Move_type::Ri, 2); solve.link0() }
  threads << Thread.new { solve = get_new_solver_with_move($g_initial_cube_str, Move_type::R, Move_type::Ri, 3); solve.link1() }

  threads << Thread.new { solve = get_new_solver_with_move($g_initial_cube_str, Move_type::R, Move_type::Ri, 0); solve.solve() }
  threads << Thread.new { solve = get_new_solver_with_move($g_initial_cube_str, Move_type::L, Move_type::Li, 0); solve.solve() }
  threads << Thread.new { solve = get_new_solver_with_move($g_initial_cube_str, Move_type::U, Move_type::Ui, 0); solve.solve() }
  threads << Thread.new { solve = get_new_solver_with_move($g_initial_cube_str, Move_type::D, Move_type::Di, 0); solve.solve() }
  threads << Thread.new { solve = get_new_solver_with_move($g_initial_cube_str, Move_type::F, Move_type::Fi, 0); solve.solve() }
  threads << Thread.new { solve = get_new_solver_with_move($g_initial_cube_str, Move_type::B, Move_type::Bi, 0); solve.solve() }

  threads << Thread.new { solve = get_new_solver_with_move($g_good_cube_str, Move_type::R, Move_type::Ri, 1); solve.solve() }
  threads << Thread.new { solve = get_new_solver_with_move($g_good_cube_str, Move_type::L, Move_type::Li, 1); solve.solve() }
  threads << Thread.new { solve = get_new_solver_with_move($g_good_cube_str, Move_type::U, Move_type::Ui, 1); solve.solve() }
  threads << Thread.new { solve = get_new_solver_with_move($g_good_cube_str, Move_type::D, Move_type::Di, 1); solve.solve() }
  threads << Thread.new { solve = get_new_solver_with_move($g_good_cube_str, Move_type::F, Move_type::Fi, 1); solve.solve() }
  threads << Thread.new { solve = get_new_solver_with_move($g_good_cube_str, Move_type::B, Move_type::Bi, 1); solve.solve() }

  threads.each { |thr| thr.join }
end

# def solve_force_with_heuristic()
#   threads = []
#
#   threads << Thread.new { solve = get_new_solver_with_move($g_initial_cube_str, Move_type::R, Move_type::Ri, 0); solve.solve_with_heuristic() }
#   threads << Thread.new { solve = get_new_solver_with_move($g_initial_cube_str, Move_type::L, Move_type::Li, 0); solve.solve_with_heuristic() }
#   threads << Thread.new { solve = get_new_solver_with_move($g_initial_cube_str, Move_type::U, Move_type::Ui, 0); solve.solve_with_heuristic() }
#   threads << Thread.new { solve = get_new_solver_with_move($g_initial_cube_str, Move_type::D, Move_type::Di, 0); solve.solve_with_heuristic() }
#   threads << Thread.new { solve = get_new_solver_with_move($g_initial_cube_str, Move_type::F, Move_type::Fi, 0); solve.solve_with_heuristic() }
#   threads << Thread.new { solve = get_new_solver_with_move($g_initial_cube_str, Move_type::B, Move_type::Bi, 0); solve.solve_with_heuristic() }
#
#   threads.each { |thr| thr.join }
# end

def main()
  parse = Parser.new
  return false if parse.check() == false
  size = $g_cube_size
  cube = Cube.new(size)

  $g_initial_cube_str = serialize(cube.cube, cube.cube_size) # example
  $g_good_cube_str = $g_initial_cube_str.dup
  return false if parse.check_syntax($g_mix_str, $g_initial_cube_str) == false
  # cube.print_cube(cube.cube, size)
  puts $g_initial_cube_str
  cube = deserialize($g_initial_cube_str, cube.cube_size) # example
  # cube.print_cube(cube.cube, size)

  case $g_mode
  when 0
    solve_pattern($g_initial_cube_str.dup)
  when 1
    solve_force()
  end
  print_move_list()
  print_subdivision() if $g_subdivision == 1

  render = Render.new
  render.main_loop($screen, cube.cube, size)

  # liste des move dans $g_solution (sous forme d'array Move::X)
  # nombre de move dans $g_solution_move_nbr
  # cube melange dans $g_initial_cube_str
  # deserialize(str, size) pour avoir un cube
  # $g_move.apply_move(str, move) pour appliquer un move
  # invert_move(move) dans la class solver return l'inverse de "move"
  # $g_cube_size pour la size du cube

end

$g_cube_size = 3
$g_mix_str = ""
$g_mode = 0 # 0 for pattern, 1 for solver brute, 2 for solver with heuristic
$g_subdivision = 0

$g_good_cube_str = ""
$g_initial_cube_str = ""

$g_solution = Array.new
$g_solution_move_nbr = -1

$g_move = Move.new
$g_semaphore = Mutex.new
$g_semaphore_link0 = Mutex.new
$g_semaphore_link_tmp0 = Mutex.new
$g_semaphore_link1 = Mutex.new
$g_semaphore_link_tmp1 = Mutex.new

$g_final_list0 = Array.new
$g_final_list_tmp0 = Array.new
$g_final_list1 = Array.new
$g_final_list_tmp1 = Array.new

main()

# "R2 D' B' D F2 R F2 R2 U L' F2 U' B' L2 R D B' R' B2 L2 F2 L2 R2 U2 D2" # final case (done)
