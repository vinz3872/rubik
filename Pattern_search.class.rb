class Pattern_search
  attr_accessor :orientation # [y, x]
  attr_accessor :is_solve
  attr_accessor :cube_state
  attr_accessor :cube_str

  def initialize(str)
    @is_solve = false
    @cube_state = 0
    @cube_str = str
    @orientation = [0, 0]
    @move_lst = Array.new
  end

  def check_if_solve()
    return true if (@cube_str <=> $g_good_cube_str) == 0

    false
  end

  # default: change_orientation(Face::O, Face::W)
  def change_orientation(face_color, up_color)
    # puts "change orientation [" + face_color.to_s + " " + up_color.to_s + "]"
    case up_color
    when Face::W
      @orientation[0] = 0
    when Face::Y
      @orientation[0] = 2
    end

    if @orientation[0] == 0
      case face_color
      when Face::G
        @orientation[1] = 1
      when Face::B
        @orientation[1] = 3
      when Face::R
        @orientation[1] = 2
      when Face::O
        @orientation[1] = 0
      end
    else
      case face_color
      when Face::G
        @orientation[1] = 1
      when Face::B
        # puts "blue"
        @orientation[1] = 3
      when Face::R
        # puts "red"
        @orientation[1] = 0
      when Face::O
        @orientation[1] = 2
      end
    end
    # puts "orientation [" +   @orientation[0].to_s + " " +   @orientation[1].to_s + "]"
  end

# need invert apply_x when Face::Y up
  def apply_x(move, orient)
    while orient > 0
      case move
      when Move_type::R
        move = Move_type::B
      when Move_type::Ri
        move = Move_type::Bi
      when Move_type::R2
        move = Move_type::B2
      when Move_type::L
        move = Move_type::F
      when Move_type::Li
        move = Move_type::Fi
      when Move_type::L2
        move = Move_type::F2
      when Move_type::F
        move = Move_type::R
      when Move_type::Fi
        move = Move_type::Ri
      when Move_type::F2
        move = Move_type::R2
      when Move_type::B
        move = Move_type::L
      when Move_type::Bi
        move = Move_type::Li
      when Move_type::B2
        move = Move_type::L2
      end
      orient -= 1
    end

    move
  end

  def apply_y(move)
    case move
    when Move_type::U
      move = Move_type::D
    when Move_type::Ui
      move = Move_type::Di
    when Move_type::U2
      move = Move_type::D2
    when Move_type::D
      move = Move_type::U
    when Move_type::Di
      move = Move_type::Bi
    when Move_type::D2
      move = Move_type::B2
    when Move_type::F
      move = Move_type::B
    when Move_type::Fi
      move = Move_type::Bi
    when Move_type::F2
      move = Move_type::B2
    when Move_type::B
      move = Move_type::F
    when Move_type::Bi
      move = Move_type::Fi
    when Move_type::B2
      move = Move_type::F2
    end

    move
  end

  # ex; apply_orientation(Move_type::R)
  def apply_orientation(move)
    # puts "apply_orientation " + move.to_s + " [ " + @orientation[0].to_s + " " + @orientation[1].to_s + " ]"
    move = apply_x(move, @orientation[1]) if @orientation[1] > 0
    # puts "orientation x: " + move.to_s
    move = apply_y(move) if @orientation[0] == 2
    # puts "apply_orientation y:" + move.to_s


    register_move(move)
    $g_move.apply_move(move, @cube_str)
  end

  # 0 before white cross ; 1 before white face ; 2 before 2nd layer
  # 3 before yellow cross ; 4 before yellow face ; 5 before good yellow corner
  # 6 before 3rd layer ; 7 cube solved
  def get_cube_state()
    state = 0
    state = 1 if cube_str_compare(@cube_str, Pattern::ONE[0]) == true
    state = 2 if cube_str_compare(@cube_str, Pattern::TWO[0]) == true
    state = 3 if cube_str_compare(@cube_str, Pattern::THREE[0]) == true
    state = 4 if cube_str_compare(@cube_str, Pattern::FOUR[0]) == true
    state = 5 if cube_str_compare(@cube_str, Pattern::FIVE[0]) == true
    state = 6 if cube_str_compare(@cube_str, Pattern::SIX[0]) == true
    # state = 7 if cube_str_compare(@cube_str, Pattern::SEVEN[0]) == true
    state = 8 if cube_str_compare(@cube_str, $g_good_cube_str) == true

    state
  end

  def register_move(move)
    # puts move
    @move_lst << move
  end

  def calc_x_times(color, face_color)
    # puts "calc_x_times " + color.to_s + " " + face_color.to_s
    case color
    when Face::O
      case face_color
      when Face::O
        return 0
      when Face::G
        return 1
      when Face::R
        return 2
      when Face::B
        return 3
      end
    when Face::G
      case face_color
      when Face::O
        return 3
      when Face::G
        return 0
      when Face::R
        return 1
      when Face::B
        return 2
      end
    when Face::R
      case face_color
      when Face::O
        return 2
      when Face::G
        return 3
      when Face::R
        return 0
      when Face::B
        return 1
      end
    when Face::B
      case face_color
      when Face::O
        return 1
      when Face::G
        return 2
      when Face::R
        return 3
      when Face::B
        return 0
      end
    end
  end

  def do_move_x_times(move1, move2, color, face_pos)
    x = calc_x_times(color, face_pos)

    if x == 1 && @orientation[0] == 2
      x = 3
    elsif x == 3  && @orientation[0] == 2
      x = 1
    end

    # puts "calc_x_time = " + x.to_s
    if x == 3
      apply_orientation(move2)
      x = 0
    end
    while x > 0
      apply_orientation(move1)
      x -= 1
    end
  end

  def move_to_pattern(array_move, color, face_pos)
    tmp_orient = [0, 0]
    array_move.each do |move|
      case move
      when Move_type::R
        apply_orientation(Move_type::R)
      when Move_type::L
        apply_orientation(Move_type::L)
      when Move_type::U
        apply_orientation(Move_type::U)
      when Move_type::D
        apply_orientation(Move_type::D)
      when Move_type::F
        apply_orientation(Move_type::F)
      when Move_type::B
        apply_orientation(Move_type::B)
      when Move_type::Ri
        apply_orientation(Move_type::Ri)
      when Move_type::Li
        apply_orientation(Move_type::Li)
      when Move_type::Ui
        apply_orientation(Move_type::Ui)
      when Move_type::Di
        apply_orientation(Move_type::Di)
      when Move_type::Fi
        apply_orientation(Move_type::Fi)
      when Move_type::Bi
        apply_orientation(Move_type::Bi)
      when Move_type::R2
        apply_orientation(Move_type::R2)
      when Move_type::L2
        apply_orientation(Move_type::L2)
      when Move_type::U2
        apply_orientation(Move_type::U2)
      when Move_type::D2
        apply_orientation(Move_type::D2)
      when Move_type::F2
        apply_orientation(Move_type::F2)
      when Move_type::B2
        apply_orientation(Move_type::B2)
      when "OD"
        tmp_orient[0] = @orientation[0]
        tmp_orient[1] = @orientation[1]
        @orientation[0] = 0
        @orientation[1] = 0
      when "OX"
        @orientation[0] = tmp_orient[0]
        @orientation[1] = tmp_orient[1]
      when "OF"
        change_orientation(face_pos, Face::W)
      when "DX"
        do_move_x_times(Move_type::Di, Move_type::D, color, face_pos)
      when "UX"
        do_move_x_times(Move_type::U, Move_type::Ui, color, face_pos)
      end
    end
  end

  def search_pattern_with_array(arr, color)
    tmp = 0
    arr.each do |p|
      for pos in 0...p[0].length
        if p[0][pos] == 'X' && color.to_s != @cube_str[pos]
          break
        elsif p[0][pos] != 'X' && p[0][pos] != '_' && p[0][pos] != @cube_str[pos]
          break
        end
        return tmp if pos == p[0].length - 1
      end
      tmp += 1
    end

    -1
  end

  # green - red - blue - orange
  def state_0()
    color = [Face::G, Face::R, Face::B, Face::O]
    face_color = [Face::O, Face::G, Face::R, Face::B]
    # puts "state_0"
    for sub_state in 0...4
      change_orientation(face_color[sub_state], Face::W)
      # puts @cube_str
      i = search_pattern_with_array(Basic_pattern::WE_U, color[sub_state])
      if i >= 0
        # puts Basic_pattern::WE_U[i][0]
        if (color[sub_state] == Basic_pattern::WE_U[i][3])
          move_to_pattern(Basic_pattern::WE_U[i][2], color[sub_state], Basic_pattern::WE_U[i][3])
        else
          move_to_pattern(Basic_pattern::WE_U[i][1], color[sub_state], Basic_pattern::WE_U[i][3])
        end
        # sub_state += 1
        next
      end
      i = search_pattern_with_array(Basic_pattern::WE_M, color[sub_state])
      if i >= 0
        # puts Basic_pattern::WE_M[i][0]
        if (color[sub_state] == Basic_pattern::WE_M[i][3])
          move_to_pattern(Basic_pattern::WE_M[i][2], color[sub_state], Basic_pattern::WE_M[i][3])
        else
          move_to_pattern(Basic_pattern::WE_M[i][1], color[sub_state], Basic_pattern::WE_M[i][3])
        end
        # sub_state += 1
        next
      end
      i = search_pattern_with_array(Basic_pattern::WE_D, color[sub_state])
      if i >= 0
        # puts Basic_pattern::WE_D[i][0]
        if (color[sub_state] == Basic_pattern::WE_D[i][3])
          move_to_pattern(Basic_pattern::WE_D[i][2], color[sub_state], Basic_pattern::WE_D[i][3])
        else
          move_to_pattern(Basic_pattern::WE_D[i][1], color[sub_state], Basic_pattern::WE_D[i][3])
        end
        # sub_state += 1
        next
      end
    end
  end

  def check_corner_color(corner, search)
    for pos in 0...search.length
      if search[pos] == 'X' && corner[0].to_s != @cube_str[pos]
        return false
      elsif search[pos] == 'Y' && corner[1].to_s != @cube_str[pos]
        return false
      elsif search[pos] == 'Z' && corner[2].to_s != @cube_str[pos]
        return false
      end
    end
  end

  def search_pattern_corner_with_array(arr, arr_color)
    tmp = 0
    arr.each do |p|
      # puts "search " + p[0]
      for pos in 0...p[0].length
        if p[0][pos] == 'X' && arr_color[0].to_s != @cube_str[pos] && arr_color[1].to_s != @cube_str[pos] && arr_color[2].to_s != @cube_str[pos]
          # puts "break at X for " + @cube_str[pos].to_s + " arr_color val = " + arr_color[0].to_s + " " + arr_color[1].to_s + " " + arr_color[2].to_s + " "
          break
        elsif p[0][pos] == 'Y' && arr_color[0].to_s != @cube_str[pos] && arr_color[1].to_s != @cube_str[pos] && arr_color[2].to_s != @cube_str[pos]
          # puts "break at Y for " + @cube_str[pos].to_s + " arr_color val = " + arr_color[0].to_s + " " + arr_color[1].to_s + " " + arr_color[2].to_s + " "
          break
        elsif p[0][pos] == 'Z' && arr_color[0].to_s != @cube_str[pos] && arr_color[1].to_s != @cube_str[pos] && arr_color[2].to_s != @cube_str[pos]
          # puts "break at Z for " + @cube_str[pos].to_s + " arr_color val = " + arr_color[0].to_s + " " + arr_color[1].to_s + " " + arr_color[2].to_s + " "
          break
        elsif p[0][pos] != 'X' && p[0][pos] != 'Y' && p[0][pos] != 'Z' && p[0][pos] != '_' && p[0][pos] != @cube_str[pos]
          # puts "break for " + p[0][pos].to_s + " vs " + @cube_str[pos].to_s
          break
        end
        return tmp if pos == p[0].length - 1
      end
      tmp += 1
    end

    -1
  end

  def state_1()
    color = [[Face::O, Face::G, Face::W], [Face::G, Face::R, Face::W], [Face::R, Face::B, Face::W], [Face::B, Face::O, Face::W]]
    face_color = [Face::O, Face::G, Face::R, Face::B]
    # puts "state_1"
    for sub_state in 0...4
      change_orientation(face_color[sub_state], Face::W)
      # puts @cube_str + " sub_state = " + sub_state.to_s
      i = search_pattern_corner_with_array(Basic_pattern::WC_U, color[sub_state])
      if i >= 0
        # puts Basic_pattern::WC_U[i][0] + " i= " + i.to_s
        if check_corner_color(Basic_pattern::WC_U[i], Basic_pattern::WC_U[i][3]) == false
          move_to_pattern(Basic_pattern::WC_U[i][1], face_color[sub_state], Basic_pattern::WC_U[i][4])
          # sub_state += 1
          next
        else
          move_to_pattern(Basic_pattern::WC_U[i][1], face_color[sub_state], Basic_pattern::WC_U[i][4])
        end
      end
      i = search_pattern_corner_with_array(Basic_pattern::WC_D, color[sub_state])
      if i >= 0
        # puts @cube_str
        # puts Basic_pattern::WC_D[i][0] + " i= " + i.to_s
        # if check_corner_color(color[sub_state], Basic_pattern::WC_D[i][3]) == true
        if face_color[sub_state] == Basic_pattern::WC_D[i][4]
          move_to_pattern(Basic_pattern::WC_D[i][2], face_color[sub_state], Basic_pattern::WC_D[i][4])
        else
          move_to_pattern(Basic_pattern::WC_D[i][1], face_color[sub_state], Basic_pattern::WC_D[i][4])
        end
        # sub_state += 1
        next
      end
    end
  end

  def search_edges(arr, face_up)
    tmp = 0
    arr.each do |p|
      # puts "search " + p[0]
      for pos in 0...p[0].length
        change_orientation(@cube_str[pos].to_i, face_up) if p[0][pos] == 'X' && Face::Y.to_s != @cube_str[pos]
        if p[0][pos] == 'X' && Face::Y.to_s == @cube_str[pos]
          # puts "break X " + Face::Y.to_s + " == " + @cube_str[pos]
          break
        elsif p[0][pos] == 'Y' && Face::Y.to_s == @cube_str[pos]
          # puts "break Y " + Face::Y.to_s + " == " + @cube_str[pos]
          break
        elsif p[0][pos] != 'X' && p[0][pos] != 'Y' && p[0][pos] != '_' && p[0][pos] != @cube_str[pos]
          # puts "break " + p[0][pos] + " != " + @cube_str[pos]
          break
        end
        return tmp if pos == p[0].length - 1
      end
      tmp += 1
    end
    # puts "end"

    -1
  end

  def get_edges_Y_orientation(pattern)
    tmp = [0, 0]
    for pos in 0...pattern.length
      if pattern[pos] == 'X'
        tmp[0] = @cube_str[pos].to_i
      elsif pattern[pos] == 'Y'
        tmp[1] = @cube_str[pos].to_i
      end
    end
    case tmp[0]
    when Face::O
      return true if tmp[1] == Face::G
      return false if tmp[1] == Face::B
    when Face::G
      return true if tmp[1] == Face::R
      return false if tmp[1] == Face::O
    when Face::R
      return true if tmp[1] == Face::B
      return false if tmp[1] == Face::G
    when Face::B
      return true if tmp[1] == Face::O
      return false if tmp[1] == Face::R
    end
  end

  def get_x_color(pattern)
    for pos in 0...pattern.length
      return @cube_str[pos] if pattern[pos] == 'X'
    end
  end

  def search_wrong_edges_place(arr, color_up)
    tmp = 0
    arr.each do |p|
      color_face = -1
      for pos in 0...p[0].length
        # change_orientation(@cube_str[pos], face_up) if p[0][pos] == 'X' && Face::Y != @cube_str[pos]
        if p[0][pos] == 'X' && Face::Y.to_s != @cube_str[pos] && arr[tmp][3].to_s != @cube_str[pos]
          color_face = @cube_str[pos].to_i
        elsif p[0][pos] != 'X' && p[0][pos] != 'Y' && p[0][pos] != '_' && p[0][pos] != @cube_str[pos]
          break
        end
        if pos == p[0].length - 1 && color_face != -1
          change_orientation(color_face, color_up)
          return tmp
        end
      end
      tmp += 1
    end

    -1
  end

  def state_2()
    # puts "state_2"
    sub_state = 0
    while sub_state < 4
      tmp_x_color = 0
      # change_orientation(face_color[sub_state], Face::Y)
      # puts @cube_str
      i = search_edges(Basic_pattern::ML_U, Face::Y)
      if i >= 0
        # puts Basic_pattern::ML_U[i][0] + " i = " + i.to_s
        tmp_x_color = get_x_color(Basic_pattern::ML_U[i][0])
        if get_edges_Y_orientation(Basic_pattern::ML_U[i][0]) == true
          # puts "move left"
          # puts "edge color " + tmp_x_color.to_s + " " + Basic_pattern::ML_U[i][3].to_s
          move_to_pattern(Basic_pattern::ML_U[i][2], tmp_x_color.to_i, Basic_pattern::ML_U[i][3])
        else
          # puts "move right"
          # puts "edge color " + tmp_x_color.to_s + " " + Basic_pattern::ML_U[i][3].to_s
          move_to_pattern(Basic_pattern::ML_U[i][1], tmp_x_color.to_i, Basic_pattern::ML_U[i][3])
        end
        sub_state += 1
        # next
      end
      if (i < 0)
        # puts @cube_str
        i = search_wrong_edges_place(Basic_pattern::ML_M, Face::Y)
        if i >= 0
          # puts Basic_pattern::ML_M[i][0] + " i = " + i.to_s
          # puts "move wrong middle edge"
          move_to_pattern(Basic_pattern::ML_M[i][1], nil, nil)
          # sub_state += 1
          # next
        else
          return
        end
      end
    end
  end

  def state_3()
    # puts "state_3"
    while get_cube_state() < 4
      # change_orientation(face_color[sub_state], Face::W)
      # puts @cube_str
      i = search_pattern_with_array(Basic_pattern::YCR, nil)
      if i >= 0
        # puts Basic_pattern::YCR[i][0]
        change_orientation(Basic_pattern::YCR[i][2], Face::Y)
        move_to_pattern(Basic_pattern::YCR[i][1], nil, nil)
      end
    end
  end

  def search_yellow_wrong_corner(arr)
    tmp = 0
    num = 0
    arr.each do |p|
      for pos in 0...p[0].length
        num += 1 if p[0][pos] == 'X' && Face::Y.to_s == @cube_str[pos]
        break if num > 2
        break if p[0][pos] != 'X' && p[0][pos] != '_' && p[0][pos] != @cube_str[pos]
        return tmp if pos == p[0].length - 1 && num == 2
      end
      tmp += 1
    end

    -1
  end

  def turn_yellow_face(face_color)
    case face_color
    when Face::O
      color = @cube_str[53].to_i
    when Face::B
      color = @cube_str[35].to_i
    when Face::R
      color = @cube_str[44].to_i
    when Face::G
      color = @cube_str[26].to_i
    end
    do_move_x_times(Move_type::U, Move_type::Ui, color, face_color)
    change_orientation(color, Face::Y)
  end

  def state_4()
    # puts "state_4"
    while get_cube_state() < 5
      # puts @cube_str
      i = search_yellow_wrong_corner(Basic_pattern::YF2)
      if i >= 0
        # puts Basic_pattern::YF2[i][0]
        change_orientation(Basic_pattern::YF2[i][2], Face::Y)
        move_to_pattern(Basic_pattern::YF2[i][1], nil, nil)
      end
      if i < 0
        i = search_pattern_with_array(Basic_pattern::YF, nil)
        if i >= 0
          # puts Basic_pattern::YF[i][0]
          change_orientation(Basic_pattern::YF[i][2], Face::Y)
          turn_yellow_face(Basic_pattern::YF[i][2]) if i < 4
          move_to_pattern(Basic_pattern::YF[i][1], nil, nil)
        end
      end
    end
  end

  def correct_2_yellow_corner()
    # color = [Face::O, Face::G, Face::R, Face::B]
    change_orientation(Face::O, Face::Y)

    state = false
    while state == false
      # puts @cube_str
      if @cube_str[42] == Face::R.to_s && @cube_str[26] == Face::G.to_s && @cube_str[44] == Face::R.to_s && @cube_str[33] == Face::B.to_s
        return Face::O
      elsif @cube_str[42] == Face::R.to_s && @cube_str[26] == Face::G.to_s && @cube_str[51] == Face::O.to_s && @cube_str[35] == Face::B.to_s
        return Face::O
      elsif @cube_str[44] == Face::R.to_s && @cube_str[33] == Face::B.to_s && @cube_str[53] == Face::O.to_s && @cube_str[24] == Face::G.to_s
        return Face::O
      elsif @cube_str[44] == Face::R.to_s && @cube_str[33] == Face::B.to_s && @cube_str[51] == Face::O.to_s && @cube_str[35] == Face::B.to_s
        return Face::G
      elsif @cube_str[44] == Face::R.to_s && @cube_str[33] == Face::B.to_s && @cube_str[53] == Face::O.to_s && @cube_str[24] == Face::G.to_s
        return Face::G
      elsif @cube_str[51] == Face::O.to_s && @cube_str[35] == Face::B.to_s && @cube_str[42] == Face::R.to_s && @cube_str[26] == Face::G.to_s
        return Face::G
      elsif @cube_str[51] == Face::O.to_s && @cube_str[35] == Face::B.to_s && @cube_str[53] == Face::O.to_s && @cube_str[24] == Face::G.to_s
        return Face::R
      elsif @cube_str[51] == Face::O.to_s && @cube_str[35] == Face::B.to_s && @cube_str[42] == Face::R.to_s && @cube_str[26] == Face::G.to_s
        return Face::R
      elsif @cube_str[53] == Face::O.to_s && @cube_str[24] == Face::G.to_s && @cube_str[44] == Face::R.to_s && @cube_str[33] == Face::B.to_s
        return Face::R
      elsif @cube_str[53] == Face::O.to_s && @cube_str[24] == Face::G.to_s && @cube_str[42] == Face::R.to_s && @cube_str[26] == Face::G.to_s
        return Face::B
      elsif @cube_str[53] == Face::O.to_s && @cube_str[24] == Face::G.to_s && @cube_str[44] == Face::R.to_s && @cube_str[33] == Face::B.to_s
        return Face::B
      elsif @cube_str[42] == Face::R.to_s && @cube_str[26] == Face::G.to_s && @cube_str[51] == Face::O.to_s && @cube_str[35] == Face::B.to_s
        return Face::B
      end
      apply_orientation(Move_type::U)
    end
  end

  def state_5()
    # puts "state_5"
    while get_cube_state() < 6
      # puts @cube_str
      face = correct_2_yellow_corner()
      change_orientation(face, Face::Y)
      move_to_pattern(Basic_pattern::GYC, nil, nil)
    end
  end

  def state_6
    # puts "state_6"
    while get_cube_state() < 7
      # puts @cube_str
      change_orientation(Face::O, Face::Y)
      change_orientation(Face::R, Face::Y) if @cube_str[52] == Face::O.to_s
      change_orientation(Face::G, Face::Y) if @cube_str[34] == Face::B.to_s
      change_orientation(Face::O, Face::Y) if @cube_str[43] == Face::R.to_s
      change_orientation(Face::B, Face::Y) if @cube_str[25] == Face::G.to_s
      if (@cube_str[52] == Face::G.to_s || @cube_str[34] == Face::O.to_s || @cube_str[43] == Face::B.to_s || @cube_str[25] == Face::R.to_s)
        # puts "right"
        move_to_pattern(Basic_pattern::FL_R, nil, nil)
      else
        # puts "left"
        move_to_pattern(Basic_pattern::FL_L, nil, nil)
      end
    end
  end

  def solve()
    # puts "solve pattern"
    @cube_state = get_cube_state()
    # puts "  cube state = " + @cube_state.to_s, @cube_str

    state_0() if @cube_state == 0
    @cube_state = get_cube_state()
    # puts "  cube state = " + @cube_state.to_s, @cube_str
    state_1() if @cube_state == 1
    @cube_state = get_cube_state()
    # puts "  cube state = " + @cube_state.to_s, @cube_str
    state_2() if @cube_state == 2
    @cube_state = get_cube_state()
    # puts "  cube state = " + @cube_state.to_s, @cube_str
    state_3() if @cube_state == 3
    @cube_state = get_cube_state()
    # puts "  cube state = " + @cube_state.to_s, @cube_str
    state_4() if @cube_state == 4
    @cube_state = get_cube_state()
    # puts "  cube state = " + @cube_state.to_s, @cube_str
    state_5() if @cube_state == 5
    @cube_state = get_cube_state()
    # puts "  cube state = " + @cube_state.to_s, @cube_str
    state_6() if @cube_state == 6
    @cube_state = get_cube_state()
    # puts "  cube state = " + @cube_state.to_s, @cube_str

    puts "Final cube state : " + @cube_state.to_s, "Cube str = " + @cube_str

    $g_solution = @move_lst
    optimize_solution()
    optimize_solution_plus()
    $g_solution_move_nbr = $g_solution.count
  end

end
