class Cube
  attr_accessor :cube_size
  attr_accessor :cube

  def initialize(size)
    @cube_size = size
    @cube = Array.new
    init_cube()
  end

  def print_cube(cube, size)
    i = 0

    while i < 6
      j = 0
      while j < cube_size
        k = 0
        while k < cube_size
          print cube[i][j][k].to_s + " "
          k += 1
        end
        puts " "
        j += 1
      end
      puts "\n \n "
      i += 1
    end
  end

  def get_line(color)
    line = Array.new
    i = 0

    while (i < @cube_size)
      line.insert(-1, color)
      i += 1
    end

  # line = Array.new(size, $test_color)

  # line[0] = rand(5)
  # line[1] = rand(5)
  # line[2] = rand(5)
    line
  end

  def get_face(color)
    face = Array.new
    i = 0

    while (i < @cube_size)
      face.insert(-1, get_line(color))
      i += 1
    end

    face
  end

  def init_cube()
    i = 0

    while i < 6
      @cube.insert(-1, get_face(i))
      i += 1
    end
  end
end
