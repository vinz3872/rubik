#!/usr/bin/env ruby
SCREEN_WIDTH = 1280
SCREEN_HEIGHT = 720

SPACE = 10
TAB_WIDTH = SCREEN_WIDTH / 8
TAB_HEIGHT = SCREEN_HEIGHT / 4

CASE0_X = TAB_WIDTH + SPACE * 2
CASE0_Y = SPACE

CASE1_X = TAB_WIDTH + SPACE * 2
CASE1_Y = TAB_HEIGHT * 2 + 3 * SPACE

CASE2_X = SPACE
CASE2_Y = TAB_HEIGHT + SPACE * 2

CASE3_X = TAB_WIDTH * 2 + SPACE * 3
CASE3_Y = TAB_HEIGHT + SPACE * 2

CASE4_X = TAB_WIDTH + SPACE * 2
CASE4_Y = TAB_HEIGHT + SPACE * 2

CASE5_X = TAB_WIDTH * 3 + SPACE * 4
CASE5_Y = TAB_HEIGHT + SPACE * 2

BIG_CASE_X = TAB_WIDTH * 5 + SPACE * 3
BIG_CASE_Y = TAB_HEIGHT / 4

EXIT_X = TAB_WIDTH * 3 + SPACE * 3
EXIT_Y = SPACE * 5


C_WHITE = 0
C_YELLOW = 1
C_GREEN = 2
C_BLUE = 3
C_RED = 4
C_ORANGE = 5
C_EXIT = 6
C_NEXT = 7
C_PREV = 8

$arrow_left = SDL::Surface.loadBMP("icon/left.bmp")
$arrow_right = SDL::Surface.loadBMP("icon/right.bmp")
$arrow_bottom = SDL::Surface.loadBMP("icon/bottom.bmp")
$arrow_up = SDL::Surface.loadBMP("icon/up.bmp")
$exit_bmp = SDL::Surface.loadBMP("icon/exit.bmp")
$arrow_left_100 = SDL::Surface.loadBMP("icon/left_100.bmp")
$arrow_right_100 = SDL::Surface.loadBMP("icon/right_100.bmp")
$arrow_up_100 = SDL::Surface.loadBMP("icon/up_100.bmp")
$arrow_bottom_100 = SDL::Surface.loadBMP("icon/bottom_100.bmp")
