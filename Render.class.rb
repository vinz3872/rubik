#!/nfs/zfs-student-3/users/2013/jmoiroux/.rvm/rubies/ruby-2.2.1/bin/ruby
#!/usr/bin/env ruby

require 'sdl'
require 'awesome_print'
load 'Define.rb'

#https://www.kmc.gr.jp/~ohai/rubysdl_ref_2.en.html
#https://www.kmc.gr.jp/~ohai/rubysdl.en.html

err = SDL.init SDL::INIT_EVERYTHING
if err
  puts err
end
$screen = SDL::set_video_mode SCREEN_WIDTH, SCREEN_HEIGHT, 24, SDL::SWSURFACE
x = y = 0

BGCOLOR = $screen.format.mapRGB 36, 25, 132
LINECOLOR = $screen.format.mapRGB 255, 255, 255

WHITE = $screen.format.mapRGB 255, 255, 255
BLUE = $screen.format.mapRGB 0, 0, 255
GREEN = $screen.format.mapRGB 0, 255, 0
ORANGE = $screen.format.mapRGB 255, 165, 0
YELLOW = $screen.format.mapRGB 255, 255, 0
RED = $screen.format.mapRGB 255, 0, 0

$color = Array.new(Array.new)
$color[C_WHITE] = WHITE
$color[C_YELLOW] = YELLOW
$color[C_GREEN] = GREEN
$color[C_BLUE] = BLUE
$color[C_RED] = RED
$color[C_ORANGE] = ORANGE

class Render
  def main_loop(tscreen, cube, size)
      running = true
      selected = 0
      index = 0
      i = 0

      move_array = $g_solution
      array_cube_solution_string = Array.new
      cube_current = $g_initial_cube_str

      array_cube_solution_string.push(cube_current)

      $g_solution.each do |move|
        result = String.new(cube_current)
        $g_move.apply_move(move_array[i], result)
        array_cube_solution_string.push(result)
        cube_current = result
        i += 1
      end

      cube_current = array_cube_solution_string.at(0)

      while running
        while event = SDL::Event2.poll
          case event
            when SDL::Event2::KeyDown
              running = false if event.sym == SDL::Key::ESCAPE

              if event.sym == SDL::Key::RIGHT
                puts "KEY RIGHT " + (index).to_s + " <> " + cube_current
                if index <  $g_solution_move_nbr
                  index += 1 if index + 1 <= $g_solution_move_nbr
                  cube_current = array_cube_solution_string.at(index)
                  puts "Move: " + move_to_str($g_solution[index - 1]) + ", new index: "+ index.to_s + ", cube: " + cube_current
                end

              elsif event.sym == SDL::Key::LEFT
                puts "KEY LEFT " + (index).to_s + " <> " + cube_current
                if index > 0
                  index -= 1 if index - 1 >= 0
                  cube_current = array_cube_solution_string.at(index)
                  puts "Move: " + move_to_str($g_move.invert_move($g_solution[index])) + ", new index: "+ index.to_s + ", cube: " + cube_current
                end
              end

              selected = 0 if event.sym == SDL::Key::KP1
              selected = 1 if event.sym == SDL::Key::KP2
              selected = 2 if event.sym == SDL::Key::KP3
              selected = 3 if event.sym == SDL::Key::KP4
              selected = 4 if event.sym == SDL::Key::KP5
              selected = 5 if event.sym == SDL::Key::KP6
              break

            when SDL::Event2::Quit
              running = false
              break

          end
        end
        cude_3d = deserialize(cube_current, $g_cube_size)
        self.draw_figure(tscreen, cude_3d.cube, size)
        self.case_selected(cude_3d.cube, size, selected)
        tscreen.flip
      end
  end

  def draw_cross(tscreen, posx, posy)
    tscreen.put($arrow_left_100, posx, posy)
    tscreen.put($arrow_right_100, posx + $arrow_left_100.w * 2 + SPACE * 4, posy)
    tscreen.put($arrow_up_100, posx + $arrow_left_100.w + SPACE * 2, posy - SPACE * 11)
    tscreen.put($arrow_bottom_100, posx + $arrow_left_100.w + SPACE * 2, posy + SPACE * 11)
  end

  def change_selected(posx, posy)
    if posx >= CASE0_X && posx <= CASE0_X + TAB_WIDTH && posy >= CASE0_Y && posy <= CASE0_Y + TAB_HEIGHT
      return 0
    elsif posx >= CASE1_X && posx <= CASE1_X + TAB_WIDTH && posy >= CASE1_Y && posy <= CASE1_Y + TAB_HEIGHT
      return 1
    elsif posx >= CASE2_X && posx <= CASE2_X + TAB_WIDTH && posy >= CASE2_Y && posy <= CASE2_Y + TAB_HEIGHT
      return 2
    elsif posx >= CASE3_X && posx <= CASE3_X + TAB_WIDTH && posy >= CASE3_Y && posy <= CASE3_Y + TAB_HEIGHT
      return 3
    elsif posx >= CASE4_X && posx <= CASE4_X + TAB_WIDTH && posy >= CASE4_Y && posy <= CASE4_Y + TAB_HEIGHT
      return 4
    elsif posx >= CASE5_X && posx <= CASE5_X + TAB_WIDTH && posy >= CASE5_Y && posy <= CASE5_Y + TAB_HEIGHT
      return 5
    elsif posx >= EXIT_X && posx <= EXIT_X + $exit_bmp.w && posy >= EXIT_Y && posy <= EXIT_Y + $exit_bmp.h
      return C_EXIT
    else
      return -1
    end
  end

  def draw_figure(tscreen, cube, size)
    self.case0(cube, size)
    self.case1(cube, size)
    self.case2(cube, size)
    self.case3(cube, size)
    self.case4(cube, size)
    self.case5(cube, size)
  end

  def case_selected(cube, size, selected)
    case selected
      when 0
        self.big_grind(BIG_CASE_X, BIG_CASE_Y, cube[0], size)
      when 1
        self.big_grind(BIG_CASE_X, BIG_CASE_Y, cube[1], size)
      when 2
        self.big_grind(BIG_CASE_X, BIG_CASE_Y, cube[3], size)
      when 3
        self.big_grind(BIG_CASE_X, BIG_CASE_Y, cube[2], size)
      when 4
        self.big_grind(BIG_CASE_X, BIG_CASE_Y, cube[5], size)
      when 5
        self.big_grind(BIG_CASE_X, BIG_CASE_Y, cube[4], size)
      when C_EXIT
        exit
    end
  end

  def big_grind(posx, posy, carre, size)
    y = x = 0
    while y < size
      x = 0
      while x < size
        $screen.fill_rect ( posx + ( x * (TAB_WIDTH / 1.5) ) ),
        ( posy + ( y * ( TAB_HEIGHT / 1.5) ) ),
        TAB_WIDTH / 1.5,
        TAB_HEIGHT / 1.5,
        $color[carre[y][x]]
        x += 1
      end
      y += 1
    end
  end

  def case0(cube, size)
    self.draw_grind(CASE0_X, CASE0_Y, cube[0], size)
  end

  def case1(cube, size)
    self.draw_grind(CASE1_X, CASE1_Y, cube[1], size)
  end
  def case2(cube, size)
    self.draw_grind(CASE2_X, CASE2_Y, cube[3], size)
  end

  def case3(cube, size)
    self.draw_grind(CASE3_X, CASE3_Y, cube[2], size)
  end

  def case4(cube, size)
    self.draw_grind(CASE4_X, CASE4_Y, cube[5], size)
  end

  def case5(cube, size)
    self.draw_grind(CASE5_X, CASE5_Y, cube[4], size)
  end

  def draw_grind(posx, posy, carre, size)
    y = x = 0
    while y < size
      x = 0
      while x < size
        $screen.fill_rect ( posx + ( x * (TAB_WIDTH / 3) ) ), ( posy + ( y * ( TAB_HEIGHT / 3) ) ), TAB_WIDTH / 3, TAB_HEIGHT / 3, $color[carre[y][x]]
        x += 1
      end
      y += 1
    end
  end

  def invert_move(move)
    case move
    when Move_type::R
      return Move_type::Ri
    when Move_type::L
      return Move_type::Li
    when Move_type::U
      return Move_type::Ui
    when Move_type::D
      return Move_type::Di
    when Move_type::F
      return Move_type::Fi
    when Move_type::B
      return Move_type::Bi
    when Move_type::Ri
      return Move_type::R
    when Move_type::Li
      return Move_type::L
    when Move_type::Ui
      return Move_type::U
    when Move_type::Di
      return Move_type::D
    when Move_type::Fi
      return Move_type::F
    when Move_type::Bi
      return Move_type::B
    when Move_type::R2
      return Move_type::R2
    when Move_type::L2
      return Move_type::L2
    when Move_type::U2
      return Move_type::U2
    when Move_type::D2
      return Move_type::D2
    when Move_type::F2
      return Move_type::F2
    when Move_type::B2
      return Move_type::B2
    end
  end

end
