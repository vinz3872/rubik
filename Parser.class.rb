# load 'Cube.class.rb'
# load 'Move.class.rb'

module Parse_move
  R = ["R", 1]
  L = ["L", 1]
  U = ["U", 1]
  D = ["D", 1]
  F = ["F", 1]
  B = ["B", 1]
  Ri = ["R'", 2]
  Li = ["L'", 2]
  Ui = ["U'", 2]
  Di = ["D'", 2]
  Fi = ["F'", 2]
  Bi = ["B'", 2]
  R2 = ["R2", 2]
  L2 = ["L2", 2]
  U2 = ["U2", 2]
  D2 = ["D2", 2]
  F2 = ["F2", 2]
  B2 = ["B2", 2]
end

class Parser
  def print_check_error(err)
    arg_err = "./Game.class.rb [-s size] [-p] \"move\" \n\t-s: define cube size\n\t-p: precise mode\n\t-g: generate mix str\n\t-sub: solution subdivision"
    size_err = "Size must be between 3 and 3"
    mix_err = "Insert a mix with at least 1 move"
    puts arg_err if err == 0
    puts size_err if err == 1
    puts mix_err if err == 2

    false
  end

  def check()
    return print_check_error(0) if ARGV.empty?
    tmp = 0
    arg_state = [0, 0, 0, 0]
    ARGV.each do |arg|
      if tmp == 0
        case arg
        when "-sub"
          return print_check_error(0) if arg_state[3] == 1
          arg_state[3] = 1
          $g_subdivision = 1
          next
        when "-s"
          tmp = 1
          return print_check_error(0) if arg_state[1] == 1
          arg_state[1] = 1
          next
        when "-g"
          return print_check_error(0) if arg_state[2] == 1
          arg_state[2] = 1
          tmp = 2
          next
        when "-p"
          $g_mode = 1
          return print_check_error(0) if arg_state[0] == 1
          arg_state[0] = 1
        else
          return print_check_error(0) if arg_state[2] == 1
          arg_state[2] = 1
          $g_mix_str = arg
          return print_check_error(2) if $g_mix_str.length < 1
        end
      else
        if tmp == 1
          return print_check_error(1) if arg.to_i != 3
          $g_cube_size = arg.to_i
          tmp = 0
        elsif tmp == 2
          return print_check_error(2) if arg.to_i <= 0
          $g_mix_str = generate_mix_str(arg.to_i).strip
          puts "Mix string: " + $g_mix_str
          tmp = 0
        end
      end
    end
    return false if tmp != 0
    return print_check_error(0) if arg_state[2] == 0

    true
  end

  # def check_file_syntax(filename)
  #   if File.exist?(filename) && File.file?(filename)
  #     File.readlines(filename).each do |line|
  #       prs_get_letter(line, content)
  #       return if $g_error == 1
  #       prs_get_rules(line, rules, question)
  #       return if $g_error == 1
  #     end
  #   else
  #     puts "Invalid file"
  #     return false
  #   end
  # end

  def string_compare(str, index, move)
    i = 0
    if index + move[1] > str.size
      return false
    end

    while i < move[1]
      return false if str[index + i] != move[0][i]
      i += 1
    end
    return true if (index + move[1] >= str.size) || (str[index + i] == " ")

    false
  end

  def check_syntax(str, cube_str)
    i = 0

    while i < str.size
      if string_compare(str, i, Parse_move::R) == true
        i += Parse_move::R[1]
        # puts "R"
        $g_move.apply_move(Move_type::R, cube_str)
      elsif string_compare(str, i, Parse_move::L) == true
        i += Parse_move::L[1]
        # puts "L"
        $g_move.apply_move(Move_type::L, cube_str)
      elsif string_compare(str, i, Parse_move::U) == true
        i += Parse_move::U[1]
        # puts "U"
        $g_move.apply_move(Move_type::U, cube_str)
      elsif string_compare(str, i, Parse_move::D) == true
        i += Parse_move::D[1]
        # puts "D"
        $g_move.apply_move(Move_type::D, cube_str)
      elsif string_compare(str, i, Parse_move::F) == true
        i += Parse_move::F[1]
        # puts "F"
        $g_move.apply_move(Move_type::F, cube_str)
      elsif string_compare(str, i, Parse_move::B) == true
        i += Parse_move::B[1]
        # puts "B"
        $g_move.apply_move(Move_type::B, cube_str)
      elsif string_compare(str, i, Parse_move::Ri) == true
        i += Parse_move::Ri[1]
        # puts "R'"
        $g_move.apply_move(Move_type::Ri, cube_str)
      elsif string_compare(str, i, Parse_move::Li) == true
        i += Parse_move::Li[1]
        # puts "L'"
        $g_move.apply_move(Move_type::Li, cube_str)
      elsif string_compare(str, i, Parse_move::Ui) == true
        i += Parse_move::Ui[1]
        # puts "U'"
        $g_move.apply_move(Move_type::Ui, cube_str)
      elsif string_compare(str, i, Parse_move::Di) == true
        i += Parse_move::Di[1]
        # puts "D'"
        $g_move.apply_move(Move_type::Di, cube_str)
      elsif string_compare(str, i, Parse_move::Fi) == true
        i += Parse_move::Fi[1]
        # puts "F'"
        $g_move.apply_move(Move_type::Fi, cube_str)
      elsif string_compare(str, i, Parse_move::Bi) == true
        i += Parse_move::Bi[1]
        # puts "B'"
        $g_move.apply_move(Move_type::Bi, cube_str)
      elsif string_compare(str, i, Parse_move::R2) == true
        i += Parse_move::R2[1]
        # puts "R2"
        $g_move.apply_move(Move_type::R2, cube_str)
      elsif string_compare(str, i, Parse_move::L2) == true
        i += Parse_move::L2[1]
        # puts "L2"
        $g_move.apply_move(Move_type::L2, cube_str)
      elsif string_compare(str, i, Parse_move::U2) == true
        i += Parse_move::U2[1]
        # puts "U2"
        $g_move.apply_move(Move_type::U2, cube_str)
      elsif string_compare(str, i, Parse_move::D2) == true
        i += Parse_move::D2[1]
        # puts "D2"
        $g_move.apply_move(Move_type::D2, cube_str)
      elsif string_compare(str, i, Parse_move::F2) == true
        i += Parse_move::F2[1]
        # puts "F2"
        $g_move.apply_move(Move_type::F2, cube_str)
      elsif string_compare(str, i, Parse_move::B2) == true
        i += Parse_move::B2[1]
        # puts "B2"
        $g_move.apply_move(Move_type::B2, cube_str)
      else
        puts "syntax error"
        return false
      end
      while i < str.size && str[i] == " "
        i += 1
      end
    end

    true
  end

end
