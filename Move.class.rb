# Move class for 3 * 3 * 3 cubes

module Face
  W = 0
  Y = 1
  G = 2
  B = 3
  R = 4
  O = 5
end

# [src face, dest face, src pos, dest pos]
module Rotation
  # R
  R_OW = [Face::O, Face::W, 3, 6, 9, 3, 6, 9]
  R_WR = [Face::W, Face::R, 3, 6, 9, 7, 4, 1]
  R_RY = [Face::R, Face::Y, 1, 4, 7, 9, 6, 3]
  R_YO = [Face::Y, Face::O, 3, 6, 9, 3, 6, 9]
  # Ri
  Ri_OY = [Face::O, Face::Y, 3, 6, 9, 3, 6, 9]
  Ri_YR = [Face::Y, Face::R, 3, 6, 9, 7, 4, 1]
  Ri_RW = [Face::R, Face::W, 1, 4, 7, 9, 6, 3]
  Ri_WO = [Face::W, Face::O, 3, 6, 9, 3, 6, 9]
   # L
  L_OY = [Face::O, Face::Y, 1, 4, 7, 1, 4, 7]
  L_YR = [Face::Y, Face::R, 1, 4, 7, 9, 6, 3]
  L_RW = [Face::R, Face::W, 3, 6, 9, 7, 4, 1]
  L_WO = [Face::W, Face::O, 1, 4, 7, 1, 4, 7]
  # Li
  Li_OW = [Face::O, Face::W, 1, 4, 7, 1, 4, 7]
  Li_WR = [Face::W, Face::R, 1, 4, 7, 9, 6, 3]
  Li_RY = [Face::R, Face::Y, 3, 6, 9, 7, 4, 1]
  Li_YO = [Face::Y, Face::O, 1, 4, 7, 1, 4, 7]
  # U
  U_OB = [Face::O, Face::B, 1, 2, 3, 1, 2, 3]
  U_BR = [Face::B, Face::R, 1, 2, 3, 1, 2, 3]
  U_RG = [Face::R, Face::G, 1, 2, 3, 1, 2, 3]
  U_GO = [Face::G, Face::O, 1, 2, 3, 1, 2, 3]
  # Ui
  Ui_OG = [Face::O, Face::G, 1, 2, 3, 1, 2, 3]
  Ui_GR = [Face::G, Face::R, 1, 2, 3, 1, 2, 3]
  Ui_RB = [Face::R, Face::B, 1, 2, 3, 1, 2, 3]
  Ui_BO = [Face::B, Face::O, 1, 2, 3, 1, 2, 3]
  # D
  D_OG = [Face::O, Face::G, 7, 8, 9, 7, 8, 9]
  D_GR = [Face::G, Face::R, 7, 8, 9, 7, 8, 9]
  D_RB = [Face::R, Face::B, 7, 8, 9, 7, 8, 9]
  D_BO = [Face::B, Face::O, 7, 8, 9, 7, 8, 9]
  # Di
  Di_OB = [Face::O, Face::B, 7, 8, 9, 7, 8, 9]
  Di_BR = [Face::B, Face::R, 7, 8, 9, 7, 8, 9]
  Di_RG = [Face::R, Face::G, 7, 8, 9, 7, 8, 9]
  Di_GO = [Face::G, Face::O, 7, 8, 9, 7, 8, 9]
  # F
  F_WG = [Face::W, Face::G, 7, 8, 9, 1, 4, 7]
  F_GY = [Face::G, Face::Y, 1, 4, 7, 3, 2, 1]
  F_YB = [Face::Y, Face::B, 1, 2, 3, 3, 6, 9]
  F_BW = [Face::B, Face::W, 3, 6, 9, 9, 8, 7]
  # Fi
  Fi_WB = [Face::W, Face::B, 7, 8, 9, 9, 6, 3]
  Fi_BY = [Face::B, Face::Y, 3, 6, 9, 1, 2, 3]
  Fi_YG = [Face::Y, Face::G, 1, 2, 3, 7, 4, 1]
  Fi_GW = [Face::G, Face::W, 1, 4, 7, 7, 8, 9]
  # B
  B_WB = [Face::W, Face::B, 1, 2, 3, 7, 4, 1]
  B_BY = [Face::B, Face::Y, 1, 4, 7, 7, 8, 9]
  B_YG = [Face::Y, Face::G, 7, 8, 9, 9, 6, 3]
  B_GW = [Face::G, Face::W, 3, 6, 9, 1, 2, 3]
  # Bi
  Bi_WG = [Face::W, Face::G, 1, 2, 3, 3, 6, 9]
  Bi_GY = [Face::G, Face::Y, 3, 6, 9, 9, 8, 7]
  Bi_YB = [Face::Y, Face::B, 7, 8, 9, 1, 4, 7]
  Bi_BW = [Face::B, Face::W, 1, 4, 7, 3, 2, 1]
end

module Move_type
  R = 0
  L = 1
  U = 2
  D = 3
  F = 4
  B = 5
  Ri = 6
  Li = 7
  Ui = 8
  Di = 9
  Fi = 10
  Bi = 11
  R2 = 12
  L2 = 13
  U2 = 14
  D2 = 15
  F2 = 16
  B2 = 17
  NONE = 18
end

class Move

  def apply_move(move, str)
    case move
    when Move_type::R
      move_R(str)
    when Move_type::L
      move_L(str)
    when Move_type::U
      move_U(str)
    when Move_type::D
      move_D(str)
    when Move_type::F
      move_F(str)
    when Move_type::B
      move_B(str)
    when Move_type::Ri
      move_Ri(str)
    when Move_type::Li
      move_Li(str)
    when Move_type::Ui
      move_Ui(str)
    when Move_type::Di
      move_Di(str)
    when Move_type::Fi
      move_Fi(str)
    when Move_type::Bi
      move_Bi(str)
    when Move_type::R2
      move_R2(str)
    when Move_type::L2
      move_L2(str)
    when Move_type::U2
      move_U2(str)
    when Move_type::D2
      move_D2(str)
    when Move_type::F2
      move_F2(str)
    when Move_type::B2
      move_B2(str)
    end
  end

  def invert_move(move)
    case move
    when Move_type::R
      return Move_type::Ri
    when Move_type::L
      return Move_type::Li
    when Move_type::U
      return Move_type::Ui
    when Move_type::D
      return Move_type::Di
    when Move_type::F
      return Move_type::Fi
    when Move_type::B
      return Move_type::Bi
    when Move_type::Ri
      return Move_type::R
    when Move_type::Li
      return Move_type::L
    when Move_type::Ui
      return Move_type::U
    when Move_type::Di
      return Move_type::D
    when Move_type::Fi
      return Move_type::F
    when Move_type::Bi
      return Move_type::B
    when Move_type::R2
      return Move_type::R2
    when Move_type::L2
      return Move_type::L2
    when Move_type::U2
      return Move_type::U2
    when Move_type::D2
      return Move_type::D2
    when Move_type::F2
      return Move_type::F2
    when Move_type::B2
      return Move_type::B2
    end
  end


# W W W W W W W W W Y Y Y Y Y Y Y Y Y G G G G G G G G G B B B B B B B B B R R R R R R R R R O O O O O O O O O
#             default view
#                 back : R
#             ____ ____ ____
#           /_1_ /_2_ /_3_ /|
#         /___ /_W_ /___ /|3|
#       /    /    /    /|2|/|
#       ---- ---- ---- 1|/| |
#      | 1  | 2  | 3  |/|B|/|
#   G   ---- ---- ----  |/| |
#      |    | O  |    |/| |/
#       ---- ---- ----  |/
#      |    |    |    |/
#       ---- ---- ----
#            down : Y


  def clockwise_face(face, str)
    save = str[face]
    str[face] = str[face + 7 - 1]
    str[face + 7 - 1] = str[face + 9 - 1]
    str[face + 9 - 1] = str[face + 3 - 1]
    str[face + 3 - 1] = save

    save = str[face + 2 - 1]
    str[face + 2 - 1] = str[face + 4 - 1]
    str[face + 4 - 1] = str[face + 8 - 1]
    str[face + 8 - 1] = str[face + 6 - 1]
    str[face + 6 - 1] = save
  end

  def counter_clockwise_face(face, str)
    save = str[face]
    str[face] = str[face + 3 - 1]
    str[face + 3 - 1] = str[face + 9 - 1]
    str[face + 9 - 1] = str[face + 7 - 1]
    str[face + 7 - 1] = save

    save = str[face + 2 - 1]
    str[face + 2 - 1] = str[face + 6 - 1]
    str[face + 6 - 1] = str[face + 8 - 1]
    str[face + 8 - 1] = str[face + 4 - 1]
    str[face + 4 - 1] = save
  end

  def do_rotate(rot, str)
    str[(rot[1] * 9) + rot[5] - 1] = str[(rot[0] * 9) + rot[2] - 1]
    str[(rot[1] * 9) + rot[6] - 1] = str[(rot[0] * 9) + rot[3] - 1]
    str[(rot[1] * 9) + rot[7] - 1] = str[(rot[0] * 9) + rot[4] - 1]
  end

  def rotate_cube(r1, r2, r3, r4, str)
    save = [str[(r4[0] * 9) + r4[2] - 1], str[(r4[0] * 9) + r4[3] - 1], str[(r4[0] * 9) + r4[4] - 1]]
    do_rotate(r1, str)
    do_rotate(r2, str)
    do_rotate(r3, str)

    str[(r4[1] * 9) + r4[5] - 1] = save[0]
    str[(r4[1] * 9) + r4[6] - 1] = save[1]
    str[(r4[1] * 9) + r4[7] - 1] = save[2]
  end

  def move_R(str)
    rotate_cube(Rotation::R_YO, Rotation::R_RY, Rotation::R_WR, Rotation::R_OW, str)
    clockwise_face(Face::G * 9, str)
  end

  def move_L(str)
    rotate_cube(Rotation::L_WO, Rotation::L_RW, Rotation::L_YR, Rotation::L_OY, str)
    clockwise_face(Face::B * 9, str)
  end

  def move_U(str)
    rotate_cube(Rotation::U_GO, Rotation::U_RG, Rotation::U_BR, Rotation::U_OB, str)
    clockwise_face(Face::W * 9, str)
  end

  def move_D(str)
    rotate_cube(Rotation::D_BO, Rotation::D_RB, Rotation::D_GR, Rotation::D_OG, str)
    clockwise_face(Face::Y * 9, str)
  end

  def move_F(str)
    rotate_cube(Rotation::F_BW, Rotation::F_YB, Rotation::F_GY, Rotation::F_WG, str)
    clockwise_face(Face::O * 9, str)
  end

  def move_B(str)
    rotate_cube(Rotation::B_GW, Rotation::B_YG, Rotation::B_BY, Rotation::B_WB, str)
    clockwise_face(Face::R * 9, str)
  end

  def move_Ri(str)
    rotate_cube(Rotation::Ri_WO, Rotation::Ri_RW, Rotation::Ri_YR, Rotation::Ri_OY, str)
    counter_clockwise_face(Face::G * 9, str)
  end

  def move_Li(str)
    rotate_cube(Rotation::Li_YO, Rotation::Li_RY, Rotation::Li_WR, Rotation::Li_OW, str)
    counter_clockwise_face(Face::B * 9, str)
  end

  def move_Ui(str)
    rotate_cube(Rotation::Ui_BO, Rotation::Ui_RB, Rotation::Ui_GR, Rotation::Ui_OG, str)
    counter_clockwise_face(Face::W * 9, str)
  end

  def move_Di(str)
    rotate_cube(Rotation::Di_GO, Rotation::Di_RG, Rotation::Di_BR, Rotation::Di_OB, str)
    counter_clockwise_face(Face::Y * 9, str)
  end

  def move_Fi(str)
    rotate_cube(Rotation::Fi_GW, Rotation::Fi_YG, Rotation::Fi_BY, Rotation::Fi_WB, str)
    counter_clockwise_face(Face::O * 9, str)
  end

  def move_Bi(str)
    rotate_cube(Rotation::Bi_BW, Rotation::Bi_YB, Rotation::Bi_GY, Rotation::Bi_WG, str)
    counter_clockwise_face(Face::R * 9, str)
  end

  def move_R2(str)
    move_R(str)
    move_R(str)
  end

  def move_L2(str)
    move_L(str)
    move_L(str)
  end

  def move_U2(str)
    move_U(str)
    move_U(str)
  end

  def move_D2(str)
    move_D(str)
    move_D(str)
  end

  def move_F2(str)
    move_F(str)
    move_F(str)
  end

  def move_B2(str)
    move_B(str)
    move_B(str)
  end

end
